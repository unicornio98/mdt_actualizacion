# Memoria de Trabajo: Actualización

Agregar descripción.

## Installation
Se requiere de [Python3](https://www.python.org/downloads/) para ejecutar el módulo. 
Además, el script [setup.py](setup.py) instalará los requisitos restantes, ejecutando lo siguiente en la línea de comandos:

```bash
python3 setup.py
```

## Utilización

```python
import foobar

foobar.pluralize('word') # returns 'words'
foobar.pluralize('goose') # returns 'geese'
foobar.singularize('phenomena') # returns 'phenomenon'
```

## Contribuciones
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Licencia
Este proyecto esta bajo una licencia tipo `insertar tipo licencia` - ver [LICENSE](LICENSE) para más detalles.