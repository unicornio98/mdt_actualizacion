# -*- coding: utf-8 -*-
import os

thelibFolder = os.path.dirname(os.path.realpath(__file__))
requirementPath = thelibFolder + '/requirements.txt'
install_requires = [] 

if os.path.isfile(requirementPath):
    with open(requirementPath) as f:
        install_requires = f.read().splitlines()

for require in install_requires:
    os.system("python3 -m pip install -U " + require + " --user")