#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""preparacion_global.py:
   Este archivo permite configura Pygame y generar variables globales a ser
   usadas en todo el proyecto. Además llama la función correspondiente que 
   extrae los datos del participante.
"""
# =============================================================================
# Imports
# =============================================================================
import pygame
import logging
import tkinter as tk
import escritor
import datos
import constantes as ctes
# =============================================================================
# Configuración logging
# =============================================================================

logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)

# =============================================================================
# Extraer datos del participante
# =============================================================================

NOMBRE, F_D_N, SEXO, GRADO, SUJETO, FORMA, DESTROY, EVALUADOR, EDAD, PROYECTO, F_D_E = datos.extraer_datos_participante()
			
# =============================================================================
# Preparación Pygame
# =============================================================================

pygame.init()
pygame.mixer.init()		
pygame.mouse.set_visible(0)

root = tk.Tk()
size = (root.winfo_screenwidth(), root.winfo_screenheight())
root.destroy()
screen = pygame.display.set_mode(size,pygame.FULLSCREEN)
font25 = pygame.font.SysFont("Calibri",25,True,True)
font70 = pygame.font.SysFont("Calibri",70,True,True)
pygame.display.set_caption("Actualizacion")

mx = screen.get_width()/2
my = screen.get_height()/2 
cent = [mx,my]

archivo, C_P = escritor.iniciar(SUJETO, PROYECTO, FORMA)
archivo_comments = ctes.COMENT_PATH + C_P + '.txt'

clock = pygame.time.Clock()
clock_absoluto = pygame.time.Clock()

logging.info('Preparación global realizada exitosamente...')