from .preparacion_global import NOMBRE, F_D_N, SEXO, GRADO, SUJETO, FORMA, DESTROY, EVALUADOR, EDAD, PROYECTO, F_D_E
from .preparacion_global import screen
from .preparacion_global import font25, font70
from .preparacion_global import mx, my, cent
from .preparacion_global import archivo, C_P, archivo_comments
from .preparacion_global import clock, clock_absoluto