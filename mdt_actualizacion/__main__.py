#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Imports
# =============================================================================
import sys
sys.dont_write_bytecode = True

import instrucciones as instr
import auxiliares as aux
import preparacion_global as gbl
import practica as pr
import experimento as exp
import constantes as ctes
# =============================================================================
# Programa principal
# =============================================================================

#Inicia conteo del tiempo total de  la prueba
gbl.clock_absoluto.tick()		

# Correr instrucciones
instr.instrucciones()

# Correr practica
pr.practica()

# Correr experimento
exp.experimento()

# Correr caja de comentarios
aux.caja_comentarios(gbl.archivo_comments)
