#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""practica.py:
   Este archivo contiene las funciones que permiten presentar la práctica en 
   pantalla. Esta se corre únicamente si la variable CORRER_PRACTCA es True, 
   ubicada en el archivo constantes.py.
"""
# =============================================================================
# Imports
# =============================================================================
import pygame
import logging
import ensayos
import escritor
import preparacion_global as gbl
import constantes as ctes
# =============================================================================
# Métodos: generales
# =============================================================================

def practica():
    ''' 
    Corre práctica en pantalla. Método general.
    '''
    if ctes.CORRER_PRACTICA:
        logging.info('Corriendo práctica...')
        imprimir_pantalla_practica()
        parametros, parametros_d = leer_csv_practica()
        correr_ensayos_practica(parametros, parametros_d)
        imprimir_pantalla_fin_practica()
        logging.info('Fin de la práctica...')
    else:
        logging.info('Práctica desactivada...')

def imprimir_pantalla_practica():
    ''' 
    Imprime pantalla inicial de práctica.
    '''
    pygame.mouse.set_visible(0)
    gbl.screen.fill(ctes.WHITE)
    text1 = gbl.font25.render(ctes.INICIAR_MSG,True,(0,0,0))
    text1_width = text1.get_width()
    gbl.screen.blit(text1,[round(gbl.mx-text1_width/2),round(gbl.my)])

    text2 = gbl.font25.render(ctes.BARRA_ESP_MSG,True,(0,0,0))
    text2_width = text2.get_width()
    gbl.screen.blit(text2,[round(gbl.mx-text2_width/2),round(gbl.my+300)])
    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    done = True
        pygame.display.flip()

def leer_csv_practica():
    ''' 
    Lee los archivos csv de práctica y práctica dirigida y guarda los parámetros en dos variables de retorno.
    '''
    #Lee el csv de práctica donde estan los datos de los ensayos de práctica
    lect = open(ctes.PRACTICE_PATH, 'r')			
    lect.readline()
    lect.readline()
    parametros = (lect.read().replace('\r','')).split('\n')
    i = 0
    for par in parametros:
        parametros[i] = par.split(',')
        i += 1
    lect.close()

    #Lee el csv de práctica dirigida donde estan los datos de los ensayos de práctica
    lect = open(ctes.DIR_PRACTICE_PATH, 'r')		
    lect.readline()
    lect.readline()
    parametros_d = (lect.read().replace('\r','')).split('\n')
    i = 0
    for par in parametros_d:
        parametros_d[i] = par.split(',')
        i += 1
    lect.close()

    return parametros, parametros_d

def correr_ensayos_practica(parametros, parametros_d):
    ''' 
    Corre ensayos correspondientes a la práctica.
    '''
    #Corre los 3 ensayos de practica
    w1 = ensayos.ensayos(parametros[0])
    datos_escritor = gbl.SUJETO+','+gbl.NOMBRE+','+gbl.F_D_N+','+gbl.EDAD+','+gbl.F_D_E+','+gbl.SEXO+','+gbl.GRADO+','+gbl.EVALUADOR+','+gbl.FORMA+','+'Act'+','+'PRACTICA1'+','+parametros[0][2]+','+parametros[0][1]+','+str(parametros[0][3])
    escritor.escribir(1,w1,datos_escritor)

    w2 = ensayos.ensayos(parametros[1])
    datos_escritor = gbl.SUJETO+','+gbl.NOMBRE+','+gbl.F_D_N+','+gbl.EDAD+','+gbl.F_D_E+','+gbl.SEXO+','+gbl.GRADO+','+gbl.EVALUADOR+','+gbl.FORMA+','+'Act'+','+'PRACTICA2'+','+parametros[1][2]+','+parametros[1][1]+','+str(parametros[1][3])
    escritor.escribir(2,w2,datos_escritor)

    w3 = ensayos.ensayos(parametros[2])
    datos_escritor = gbl.SUJETO+','+gbl.NOMBRE+','+gbl.F_D_N+','+gbl.EDAD+','+gbl.F_D_E+','+gbl.SEXO+','+gbl.GRADO+','+gbl.EVALUADOR+','+gbl.FORMA+','+'Act'+','+'PRACTICA3'+','+parametros[2][2]+','+parametros[2][1]+','+str(parametros[2][3])
    escritor.escribir(3,w3,datos_escritor)

    completo1 = 0

    validez_t_1 = int(w1[8][0])
    validez_t_2 = int(w2[8][0]) + int(w2[8][1])
    validez_t_3 = int(w3[8][0]) + int(w3[8][1]) + int(w3[8][2])
    validez_t = validez_t_1 + validez_t_2 + validez_t_3
    res_t_1 = int(w1[9][0])
    res_t_2 = int(w2[9][0]) + int(w2[9][1])
    res_t_3 = int(w3[9][0]) + int(w3[9][1]) + int(w3[9][2])
    res_t = res_t_1 + res_t_2 + res_t_3

    if validez_t >= 3 and res_t >= 1:
        completo1 = 1

    if completo1 != 1:
        pygame.mouse.set_visible(0)
        gbl.screen.fill(ctes.WHITE)
        text1 = gbl.font25.render(ctes.LLAMAR_INV_MSG,True,(0,0,0))
        text1_width = text1.get_width()
        gbl.screen.blit(text1,[gbl.mx-text1_width/2,gbl.my])
        done = False
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_y:
                        done = True
            pygame.display.flip()

    if completo1 !=1:
        completo1 = 0
        instr.instrucciones()
        pygame.mouse.set_visible(0)
        gbl.screen.fill(ctes.WHITE)
        text1 = gbl.font25.render(ctes.BARRA_ESP_PR_MSG,True,(0,0,0))
        text1_width = text1.get_width()
        gbl.screen.blit(text1,[gbl.mx-text1_width/2,gbl.my])
        done = False
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        done = True
            pygame.display.flip()

        w1_d = ensayos.ensayos(parametros_d[0])
        datos_escritor = gbl.SUJETO+','+gbl.NOMBRE+','+gbl.F_D_N+','+gbl.EDAD+','+gbl.F_D_E+','+gbl.SEXO+','+gbl.GRADO+','+gbl.EVALUADOR+','+gbl.FORMA+','+'Act'+','+'PRACTICA1'+','+parametros_d[0][2]+','+parametros_d[0][1]+','+str(parametros_d[0][3])
        escritor.escribir(1,w1_d,datos_escritor)
        w2_d = ensayos.ensayos(parametros_d[1])
        datos_escritor = gbl.SUJETO+','+gbl.NOMBRE+','+gbl.F_D_N+','+gbl.EDAD+','+gbl.F_D_E+','+gbl.SEXO+','+gbl.GRADO+','+gbl.EVALUADOR+','+gbl.FORMA+','+'Act'+','+'PRACTICA2'+','+parametros_d[1][2]+','+parametros_d[1][1]+','+str(parametros_d[1][3])
        escritor.escribir(2,w2_d,datos_escritor)
        w3_d = ensayos.ensayos(parametros_d[2])
        datos_escritor = gbl.SUJETO+','+gbl.NOMBRE+','+gbl.F_D_N+','+gbl.EDAD+','+gbl.F_D_E+','+gbl.SEXO+','+gbl.GRADO+','+gbl.EVALUADOR+','+gbl.FORMA+','+'Act'+','+'PRACTICA3'+','+parametros_d[2][2]+','+parametros_d[2][1]+','+str(parametros_d[2][3])
        escritor.escribir(3,w3_d,datos_escritor)

        validez_t_1 = int(w1_d[8][0])
        validez_t_2 = int(w2_d[8][0]) + int(w2_d[8][1])
        validez_t_3 = int(w3_d[8][0]) + int(w3_d[8][1]) + int(w3_d[8][2])
        validez_t = validez_t_1 + validez_t_2 + validez_t_3
        res_t_1 = int(w1_d[9][0])
        res_t_2 = int(w2_d[9][0]) + int(w2_d[9][1])
        res_t_3 = int(w3_d[9][0]) + int(w3_d[9][1]) + int(w3_d[9][2])
        res_t = res_t_1 + res_t_2 + res_t_3

        if validez_t >= 3 and res_t >= 1:
            completo1 = 1

        if completo1 == 0:
            pygame.mouse.set_visible(0)
            gbl.screen.fill(ctes.WHITE)
            text1 = gbl.font25.render(ctes.GRACIAS_MSG,True,(0,0,0))
            text1_width = text1.get_width()
            gbl.screen.blit(text1,[gbl.mx-text1_width/2,gbl.my])
            done = False
            while not done:
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_y:
                            done = True
                pygame.display.flip()
            sys.exit()

def imprimir_pantalla_fin_practica():
    ''' 
    Imprime pantalla final de la práctica.
    '''
    pygame.mouse.set_visible(0)  
    gbl.screen.fill(ctes.WHITE)
    text3 = gbl.font25.render(ctes.FIN_MSG,True,(0,0,0))
    text3_width = text3.get_width()
    gbl.screen.blit(text3,[round(gbl.mx-text3_width/2),round(gbl.my)])

    text4 = gbl.font25.render(ctes.BARRA_ESP_INI,True,(0,0,0))
    text4_width = text4.get_width()
    gbl.screen.blit(text4,[round(gbl.mx-text4_width/2),round(gbl.my+300)])
    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    done = True
        pygame.display.flip()