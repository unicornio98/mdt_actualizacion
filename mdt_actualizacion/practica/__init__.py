from .practica import practica
from .practica import imprimir_pantalla_practica
from .practica import leer_csv_practica
from .practica import correr_ensayos_practica
from .practica import imprimir_pantalla_fin_practica