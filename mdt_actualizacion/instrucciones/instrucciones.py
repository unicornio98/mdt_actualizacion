#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""instrucciones.py:
   Este archivo contiene las funciones que permiten presentar las instrucciones
   en pantalla. Esta se corre únicamente si la variable CORRER_INSTRUCCIONES es
   True, ubicada en el archivo constantes.py.
"""
# =============================================================================
# Imports
# =============================================================================
import pygame
import logging
from pygame.locals import *
import ensayos
import auxiliares as aux
import coordenadas as coord
import preparacion_global as gbl
import constantes as ctes
# =============================================================================
# Constantes
# =============================================================================

BV = pygame.image.load(ctes.BV_PATH)

# =============================================================================
# Métodos: generales
# =============================================================================

def instrucciones():
	'''
	Presenta instrucciones en pantalla. Método general. 
	'''
	if ctes.CORRER_INSTRUCCIONES:
		logging.info('Corriendo instrucciones...')
		lect = open(ctes.INSTR_PATH, 'r')
		lect.readline()
		lect.readline()
		parametros_i = (lect.read().replace('\r','')).split('\n')

		i = 0
		for par in parametros_i:
			parametros_i[i] = par.split(',')
			i += 1

		lect.close()

		mx = gbl.cent[0]
		my = gbl.cent[1]
		
		pygame.mouse.set_visible(0)

		text1 = gbl.font70.render(ctes.INSTR_MSG,True,(0,0,255))
		text1_width = text1.get_width()
		gbl.screen.fill(ctes.WHITE)
		gbl.screen.blit(text1,[mx-text1_width/2,my])

		pygame.display.flip()

		done = False
		pygame.event.clear()
		while not done:
			for event in pygame.event.get():
				if event.type == pygame.KEYDOWN:
					if event.key == pygame.K_SPACE:
						done = True

		ensayos_i(parametros_i[0])
		logging.info('Fin de las instrucciones...')
	else:
		logging.info('Instrucciones desactivadas...')

def actualizacion_i(POSICION_BOLA,POSICION_CAMBIO, FLECHA, n,carga):
	'''
	Se encarga de mostrar la bola y la flecha y verificar el lugar donde el participante hace clic. Ejecuta el sonido de correcto o incorrecto de acuerdo a la respuesta.
	'''

	POS_A = int(POSICION_BOLA[n-1])
	POS_N = int (POSICION_CAMBIO[n-1])
	c= carga
	mx = gbl.cent[0]
	my = gbl.cent[1]
	centro = coord.identificar_coord(POS_N,gbl.cent)
	done = False

	gbl.screen.fill(ctes.WHITE)
	pygame.mouse.set_visible(0)
	gbl.screen.blit(BV, (coord.identificar_coord(POS_A,gbl.cent)[0]-55,coord.identificar_coord(POS_A,gbl.cent)[1]-55))
	aux.grilla(gbl.screen,gbl.cent)
	pygame.display.flip()
	done = False
	pygame.event.clear()
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_SPACE:
					done = True	

	flecha = pygame.image.load(ctes.FLECHAS_PATH + ctes.BANCO_FLECHAS[(FLECHA[n-1])])
	pygame.event.clear()
	pygame.mouse.set_visible(1)
	pygame.mouse.set_pos((mx,my))
	gbl.screen.fill(ctes.WHITE)
	gbl.screen.blit(flecha,(mx-55,my-55))
	aux.grilla(gbl.screen,gbl.cent)
	pygame.event.clear()
	done = False
	pygame.event.clear()
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.MOUSEBUTTONDOWN:
				if event.button == 1:
					done = True	
					position = event.pos
					button = event.button						
					if not (position[0] >= centro[0]-55 and position[0] <= centro[0]+55 and position[1] >= centro[1]-55 and position[1] <= centro[1]+55):
						ensayos.play_sound('n')
					else:
						ensayos.play_sound('p')
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_e:
					#Si se presiona la tecla E mientras se muestra una flecha se pasa a la pantalla de salida.
					ensayos.cerrar()
					pygame.event.clear()
					pygame.mouse.set_visible(1)
					pygame.mouse.set_pos((mx,my))
					gbl.screen.fill(ctes.WHITE)
					gbl.screen.blit(flecha,(mx-55,my-55))
					aux.grilla(gbl.screen,gbl.cent)
		pygame.display.flip()
		
	gbl.screen.fill(ctes.WHITE)
	pygame.mouse.set_visible(0)
	aux.grilla(gbl.screen,gbl.cent)
	pygame.display.flip()
	pygame.event.pump()
	pygame.time.delay(int(1000))
	
	
def mostrar_estimulo_i(escogidos, numero, posicion_estimulo):
	'''
	Funcion que muestra un estimulo dentro de la grilla, junto a un enunciado con la instruccion. 
	Lo muestra por un periodo de 2000ms
	'''
	pygame.mouse.set_visible(0)
	estimulo = pygame.image.load(ctes.ESTIMULOS_PATH +  escogidos[numero])
	pygame.event.clear()
	gbl.screen.fill(ctes.WHITE)
	gbl.screen.blit(estimulo, (coord.identificar_coord(int(posicion_estimulo),gbl.cent)[0]-55,coord.identificar_coord(int(posicion_estimulo),gbl.cent)[1]-55))
	aux.grilla(gbl.screen,gbl.cent)
	pygame.display.flip()
	done = False
	pygame.event.clear()
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_SPACE:
					done = True
	

def recall_i(escogidos, numero, casilla_estimulo):
	'''
	Funcion encargada de preguntar por la posicion del estimulo codificado, no tiene timeout.
	Retorna el resultado del recall_i, el tiempo de respuesta y por ultimo la posicion de donde hizo clic.
	'''
	mx = gbl.cent[0]
	my = gbl.cent[1]
	
	pygame.mouse.set_visible(1)
	pygame.mouse.set_pos((mx,my))
	
	estimulo = pygame.image.load(ctes.ESTIMULOS_PATH + escogidos[numero])
	pregunta = pygame.image.load(ctes.PREGUNTA_PATH)
	text1 = gbl.font25.render(ctes.PREGUNTA_FIG_MSG,True,(0,0,0))
	text2 = gbl.font25.render(ctes.CLIC_FIG,True,(0,0,0))
	text1_width = text1.get_width()
	text2_width = text2.get_width()
	pygame.event.clear()
	pygame.mouse.set_visible(1)
	pygame.mouse.set_pos((mx,my))
	gbl.screen.fill(ctes.WHITE)
	gbl.screen.blit(estimulo,(coord.identificar_coord(13,gbl.cent)[0]-55, coord.identificar_coord(13,gbl.cent)[1]-55))
	gbl.screen.blit(pregunta,(coord.identificar_coord(14,gbl.cent)[0]-55, coord.identificar_coord(14,gbl.cent)[1]-55))
	gbl.screen.blit(text1,[mx-text1_width/2,my-325])
	gbl.screen.blit(text2,[mx-text2_width/2,my+300])

	aux.grilla(gbl.screen,gbl.cent)
	done = False
	pygame.event.clear()
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.MOUSEBUTTONDOWN:
				if event.button == 1:
					done = True
					position = event.pos
					button = event.button					
					if coord.identificar_casilla(position,gbl.cent) == casilla_estimulo:
						resultados_recall_i = '1'
					else:
						resultados_recall_i ='0'
		pygame.display.flip()			
	return resultados_recall_i

def feedback_i(demanda, escogidos, list_resultados_recall_i):
	'''
	Funcion encargada de dar una retroalimentacion al partipante. 
	Muestra al estimulo por el que se preguntó y un check si fue correcto y una equis si fue incorrecto. 
	'''
	mx = gbl.cent[0]
	my = gbl.cent[1]
	
	check = pygame.image.load(ctes.CHECK_PATH)
	equis = pygame.image.load(ctes.EQUIS_PATH)
	igual = pygame.image.load(ctes.IGUAL_PATH)
	estimulo1 = pygame.image.load(ctes.ESTIMULOS_PATH + escogidos[0])
	estimulo2 = pygame.image.load(ctes.ESTIMULOS_PATH + escogidos[1])
	if demanda == 3:
		estimulo3 = pygame.image.load(ctes.ESTIMULOS_PATH + escogidos[2])
	
	pygame.mouse.set_visible(0)
	gbl.screen.fill(ctes.WHITE)
	resultados_im = []
	for i in list_resultados_recall_i:
		if i == '1':
			resultados_im.append(check)
		elif i == '0':
			resultados_im.append(equis)
			
	if demanda == 2:
		gbl.screen.blit(estimulo1,(coord.identificar_coord(7,gbl.cent)[0]-55, coord.identificar_coord(7,gbl.cent)[1]-55))
		gbl.screen.blit(estimulo2,(coord.identificar_coord(17,gbl.cent)[0]-55, coord.identificar_coord(17,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(8,gbl.cent)[0]-55, coord.identificar_coord(8,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(18,gbl.cent)[0]-55, coord.identificar_coord(18,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[0],(coord.identificar_coord(9,gbl.cent)[0]-55, coord.identificar_coord(9,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[1],(coord.identificar_coord(19,gbl.cent)[0]-55, coord.identificar_coord(19,gbl.cent)[1]-55))
	elif demanda == 3:
		gbl.screen.blit(estimulo1,(coord.identificar_coord(2,gbl.cent)[0]-55, coord.identificar_coord(2,gbl.cent)[1]-55))
		gbl.screen.blit(estimulo2,(coord.identificar_coord(12,gbl.cent)[0]-55, coord.identificar_coord(12,gbl.cent)[1]-55))
		gbl.screen.blit(estimulo3,(coord.identificar_coord(22,gbl.cent)[0]-55, coord.identificar_coord(22,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(3,gbl.cent)[0]-55, coord.identificar_coord(3,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(13,gbl.cent)[0]-55, coord.identificar_coord(13,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(23,gbl.cent)[0]-55, coord.identificar_coord(23,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[0],(coord.identificar_coord(4,gbl.cent)[0]-55, coord.identificar_coord(4,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[1],(coord.identificar_coord(14,gbl.cent)[0]-55, coord.identificar_coord(14,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[2],(coord.identificar_coord(24,gbl.cent)[0]-55, coord.identificar_coord(24,gbl.cent)[1]-55))
	
	text1 = gbl.font25.render(ctes.BARRA_ESP_CONT,True,(0,0,0))
	text1_width = text1.get_width()
	gbl.screen.blit(text1,[mx-text1_width/2,my+300])	
	
	pygame.display.flip()
	done = False
	pygame.event.clear()
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_SPACE:
					done = True
	
def ensayos_i(dat):
	'''
	Funcion encargada de corrrer en si todo la prueba, recibe los datos del excel donde se encuentra la informacion de la prueba. 
	Ademas establece variables donde se van a guardar los datos para el excel con los resultados de la prueba. 
	Esta función utiliza a las funciones anteriores para la ejecucion de la prueba total. Retorna todos los datos obtenidos por cada ensayo.
	'''
	demanda = int(dat[1])
	carga = float(dat[3])
	
	ESTIMULOS = [dat[4],dat[15],dat[26],dat[37]]
	POS_ESTIMULOS = [dat[5],dat[16],dat[27],dat[38]]
	T_CODIFICACION = ['null','null','null','null']
	POSICION_BOLA = [[dat[6],dat[8],dat[10]],[dat[17],dat[19],dat[21]],[dat[28],dat[30],dat[32]],[dat[39],dat[41],dat[43]]]
	POSICION_CAMBIO =  [[dat[7],dat[9],dat[11]],[dat[18],dat[20],dat[22]],[dat[29],dat[31],dat[33]],[dat[40],dat[42],dat[44]]]
	FLECHAS = [[dat[12],dat[13],dat[14]],[dat[23],dat[24],dat[25]],[dat[34],dat[35],dat[36]],[dat[45],dat[46],dat[47]]]
	RESULTADOS_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	T_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	CASILLAS_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	#POSICION_CLICK_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	POSICION_CLICK_ACT = [[['null','null'],['null','null'],['null','null']],[['null','null'],['null','null'],['null','null']],[['null','null'],['null','null'],['null','null']],[['null','null'],['null','null'],['null','null']]]
	VALIDEZ = ['null','null','null','null']
	RESULTADOS_recall_i = ['null','null','null','null']
	T_recall_i = ['null','null','null','null']
	CASILLAS_recall_i = ['null','null','null','null']
	
	posicion_escogidas_1 = [ctes.BANCO_POSICIONES[flecha] for flecha in FLECHAS[0]]
	posicion_escogidas_2 = [ctes.BANCO_POSICIONES[flecha] for flecha in FLECHAS[1]]
	if demanda == 3:
		posicion_escogidas_3 = [ctes.BANCO_POSICIONES[flecha] for flecha in FLECHAS[2]]
	
	mostrar_estimulo_i(ESTIMULOS, 0, POS_ESTIMULOS[0])

	ensayos.punto_de_fijacion('blanco')		 

	act_1 = actualizacion_i(POSICION_BOLA[0],POSICION_CAMBIO[0],posicion_escogidas_1,1,carga)
	act_2 = actualizacion_i(POSICION_BOLA[0],POSICION_CAMBIO[0],posicion_escogidas_1,2,carga)
	act_3 = actualizacion_i(POSICION_BOLA[0],POSICION_CAMBIO[0],posicion_escogidas_1,3,carga)
					
	if demanda == 2 or demanda == 3:
		ensayos.punto_de_fijacion('blanco')
		
	mostrar_estimulo_i(ESTIMULOS, 1, POS_ESTIMULOS[1])
		
	ensayos.punto_de_fijacion('blanco')		 
		
	act_1 = actualizacion_i(POSICION_BOLA[1],POSICION_CAMBIO[1],posicion_escogidas_2,1,carga)
	act_2 = actualizacion_i(POSICION_BOLA[1],POSICION_CAMBIO[1],posicion_escogidas_2,2,carga)
	act_3 = actualizacion_i(POSICION_BOLA[1],POSICION_CAMBIO[1],posicion_escogidas_2,3,carga)
					
	if demanda == 3:
			ensayos.punto_de_fijacion('blanco')

#Si la demanda del ensayo es 3 ejecuta esta ciclo.		
	if demanda == 3:	
		mostrar_estimulo_i(ESTIMULOS, 2, POS_ESTIMULOS[2])
	
		ensayos.punto_de_fijacion('blanco')		 
		
		act_1 = actualizacion_i(POSICION_BOLA[2],POSICION_CAMBIO[2],posicion_escogidas_2,1,carga)
		act_2 = actualizacion_i(POSICION_BOLA[2],POSICION_CAMBIO[2],posicion_escogidas_2,2,carga)
		act_3 = actualizacion_i(POSICION_BOLA[2],POSICION_CAMBIO[2],posicion_escogidas_2,3,carga)
	
	rec = recall_i(ESTIMULOS, 0, int(POS_ESTIMULOS[0]))
	RESULTADOS_recall_i[0] = rec

	rec = recall_i(ESTIMULOS, 1, int(POS_ESTIMULOS[1]))
	RESULTADOS_recall_i[1] = rec
	
	if demanda == 3:
		rec = recall_i(ESTIMULOS, 2, int(POS_ESTIMULOS[2]))
		RESULTADOS_recall_i[2] = rec
	
	if demanda == 2:
		feedback_i(2, ESTIMULOS, [RESULTADOS_recall_i[0],RESULTADOS_recall_i[1]])
	else:
		feedback_i(3, ESTIMULOS, RESULTADOS_recall_i)