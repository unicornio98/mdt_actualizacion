from .instrucciones import actualizacion_i
from .instrucciones import mostrar_estimulo_i
from .instrucciones import recall_i
from .instrucciones import feedback_i
from .instrucciones import ensayos_i
from .instrucciones import instrucciones