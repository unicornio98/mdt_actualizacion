#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""coordenadas.py:
   Este archivo contiene funciones que identifican las coordenadas asociadas a 
   cada casilla de la grilla.
"""
# =============================================================================
# Métodos: genenerales
# =============================================================================
def identificar_coord(n,cent):
	'''
	Función que identifica las coordenadas asociadas a cada casilla, las coordenas corresponden a la esquina superior izquierda de la casilla 
	le asigna al cuadrado 'n' las coordenadas en el canvas del centro de la casilla a la que corresponde.
	Use:
		n: número de la cuadricula (1-25), 
		cent:  coordenas del centro 
	
	
	Note: Cada condicional a partir del numero de la casilla, devuelve las coordenas de la esquina superior izquierda de la casilla
	'''
	mx = cent[0]
	my = cent[1]
	
	if n == 1:
		coord = [mx-220,my-220]
	elif n == 2:
		coord = [mx-110,my-220]
	elif n == 3:
		coord = [mx,my-220]
	elif n == 4:
		coord = [mx+110,my-220]
	elif n == 5 :
		coord = [mx+220,my-220]

	elif n == 6:
		coord = [mx-220,my-110]
	elif n == 7:
		coord = [mx-110,my-110]
	elif n == 8: 
		coord = [mx,my-110]
	elif n == 9:
		coord = [mx+110,my-110]
	elif n == 10:
		coord = [mx+220,my-110]

	elif n == 11:
		coord = [mx-220,my]
	elif n == 12:
		coord = [mx-110,my]
	elif n == 13 :
		coord = [mx,my]
	elif n == 14: 
		coord = [mx+110,my]
	elif n == 15:
		coord = [mx+220,my]

	elif n == 16:
		coord = [mx-220,my+110]
	elif n == 17:
		coord = [mx-110,my+110]
	elif n == 18:
		coord = [mx,my+110]
	elif n == 19:
		coord = [mx+110,my+110]
	elif n == 20:
		coord = [mx+220,my+110]

	elif n == 21:
		coord = [mx-220,my+220]
	elif n == 22:
		coord = [mx-110,my+220]
	elif n == 23:
		coord = [mx,my+220]
	elif n == 24:
		coord = [mx+110,my+220]
	elif n == 25:
		coord = [mx+220,my+220]
	return coord 

def identificar_casilla(position,cent):
	'''
	Función que retorna el numero de casilla dadas las coordenadas en la pantalla.
	Utilizada para identificar si el click que da el sujeto esta dentro de cual casilla.
	Cada condicional determina a que casilla corresponden las coordenadas que le entra la función.
	
	Use:
		position:  arreglo con [x,y] coordenadas 
		cent: coordenadas del centro de la pantalla
	
	Return:
		c: casilla correspondiente a la coordenadas 
	
	'''
	mx = cent[0]
	my = cent[1]
	
	if position == 'None':
		#Retorna 0 si no hubo click
		c = 0
	elif position[0]<(mx-220)+55 and position[0]> (mx-220)-55 and position[1]< (my-220)+55 and position[1]> (my-220)-55:
		c = 1
	elif position[0]<(mx-110)+55 and position[0]> (mx-110)-55 and position[1]< (my-220)+55 and position[1]>(my-220)-55:
		c = 2
	elif position[0]<(mx)+55 and position[0]> (mx)-55 and position[1]< (my-220)+55 and position[1]>(my-220)-55:
		c = 3
	elif position[0]<(mx+110)+55 and position[0]> (mx+110)-55 and position[1]< (my-220)+55 and position[1]>(my-220)-55:
		c = 4
	elif position[0]<(mx+220)+55 and position[0]> (mx+220)-55 and position[1]< (my-220)+55 and position[1]>(my-220)-55:
		c = 5 
	
	elif position[0]<(mx-220)+55 and position[0]> (mx-220)-55 and position[1]< (my-110)+55 and position[1]> (my-110)-55:
		c = 6
	elif position[0]<(mx-110)+55 and position[0]> (mx-110)-55 and position[1]< (my-110)+55 and position[1]>(my-110)-55:
		c = 7
	elif position[0]<(mx)+55 and position[0]> (mx)-55 and position[1]< (my-110)+55 and position[1]>(my-110)-55:
		c = 8
	elif position[0]<(mx+110)+55 and position[0]> (mx+110)-55 and position[1]< (my-110)+55 and position[1]>(my-110)-55:
		c = 9
	elif position[0]<(mx+220)+55 and position[0]> (mx+220)-55 and position[1]< (my-110)+55 and position[1]>(my-110)-55:
		c = 10

	elif position[0]<(mx-220)+55 and position[0]> (mx-220)-55 and position[1]< (my)+55 and position[1]> (my)-55:
		c = 11
	elif position[0]<(mx-110)+55 and position[0]> (mx-110)-55 and position[1]< (my)+55 and position[1]>(my)-55:
		c = 12
	elif position[0]<(mx)+55 and position[0]> (mx)-55 and position[1]< (my)+55 and position[1]>(my)-55:
		c = 13
	elif position[0]<(mx+110)+55 and position[0]> (mx+110)-55 and position[1]< (my)+55 and position[1]>(my)-55:
		c = 14
	elif position[0]<(mx+220)+55 and position[0]> (mx+220)-55 and position[1]< (my)+55 and position[1]>(my)-55:
		c = 15

	elif position[0]<(mx-220)+55 and position[0]> (mx-220)-55 and position[1]< (my+110)+55 and position[1]> (my+110)-55:
		c = 16
	elif position[0]<(mx-110)+55 and position[0]> (mx-110)-55 and position[1]< (my+110)+55 and position[1]>(my+110)-55:
		c = 17
	elif position[0]<(mx)+55 and position[0]> (mx)-55 and position[1]< (my+110)+55 and position[1]>(my+110)-55:
		c = 18
	elif position[0]<(mx+110)+55 and position[0]> (mx+110)-55 and position[1]< (my+110)+55 and position[1]>(my+110)-55:
		c = 19
	elif position[0]<(mx+220)+55 and position[0]> (mx+220)-55 and position[1]< (my+110)+55 and position[1]>(my+110)-55:
		c = 20
		
	elif position[0]<(mx-220)+55 and position[0]> (mx-220)-55 and position[1]< (my+220)+55 and position[1]> (my+220)-55:
		c = 21
	elif position[0]<(mx-110)+55 and position[0]> (mx-110)-55 and position[1]< (my+220)+55 and position[1]>(my+220)-55:
		c = 22
	elif position[0]<(mx)+55 and position[0]> (mx)-55 and position[1]< (my+220)+55 and position[1]>(my+220)-55:
		c = 23
	elif position[0]<(mx+110)+55 and position[0]> (mx+110)-55 and position[1]< (my+220)+55 and position[1]>(my+220)-55:
		c = 24
	elif position[0]<(mx+220)+55 and position[0]> (mx+220)-55 and position[1]< (my+220)+55 and position[1]>(my+220)-55:
		c = 25
	else:
		c = 'fuera de grilla'
	return c