#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""experimento.py:
   Este archivo contiene las funciones que permiten presentar los ensayos a ser
   evaluados en pantalla. Esta se corre únicamente si la variable 
   CORRER_EXPERIMENTO es True, ubicada en el archivo constantes.py.
"""
# =============================================================================
# Imports
# =============================================================================
import pygame
import os
from random import choice
import ensayos
import logging
import escritor
import preparacion_global as gbl
import constantes as ctes
# =============================================================================
# Métodos: generales
# =============================================================================

def experimento():
    '''
    Corre experimento, mostrando uno a uno los ensayos en pantalla de manera aleatoria. Método general.
    '''
    if ctes.CORRER_EXPERIMENTO:
        logging.info('Corriendo experimento...')
        p = leer_csv_ensayos()
        parametros = ordenar_ensayos(p)
        correr_ensayos_experimento(parametros)
        guardar_tiempo_total()
        imprimir_pantalla_final()
        logging.info('Fin del experimento...')
    else:
        logging.info('Experimento desactivado...')

def leer_csv_ensayos():
    '''
    Abre, lee y ordena en una lista la informacion de los ensayos a realizar.
    Esta informacion se encuentra en Generador/ensayos/ensayos-1.csv
    '''
    lect = open(ctes.ENSAYOS1_PATH, 'r')
    lect.readline()
    lect.readline()

    p = (lect.read().replace('\r','')).split('\n')
    i = 0
    for par in p:
        p[i] = par.split(',')
        i += 1
    lect.close()

    return p

def ordenar_ensayos(p):
    '''
    En este ciclo acomoda los ensayos que tiene que correr a razon de la forma escogida.
    if gbl.FORMA == '1':
        orden = [1, 17, 16, 6, 8, 19, 14, 15, 21, 12, 27, 25, 13, 11, 10, 24, 9, 23, 5, 3, 20, 7, 26, 22, 4, 28, 18, 2]

    elif gbl.FORMA == '2':
        orden = [28, 31, 21, 3, 4, 26, 30, 1, 2, 7, 27, 5, 35, 12, 23, 22, 9, 36, 32, 29, 34, 6,33, 25, 10, 11, 24, 8]

    elif gbl.FORMA == '3':
        orden = [16, 34, 14, 11, 5, 35, 7, 31, 19, 6, 32, 4, 30, 20, 29, 17, 8, 2, 15, 36, 12, 3, 13, 1, 9, 33, 18, 10]
    '''
    parametros = []
    i = 0

    while i <= 7:
        parametros.append(p[i])
        i += 1
    if gbl.FORMA == '1':
        #Set A
        i = 8
        while i <= 15:
            parametros.append(p[i])
            i += 1
        #Set B
        i = 16
        while i <= 23:
            parametros.append(p[i])
            i += 1
    elif gbl.FORMA == '2':
        #Set B
        i = 16
        while i <= 23:
            parametros.append(p[i])
            i += 1
        #Set C
        i = 24
        while i <= 31:
            parametros.append(p[i])
            i += 1
    elif gbl.FORMA == '3':
        #Set A
        i = 8
        while i <= 15:
            parametros.append(p[i])
            i += 1
        #Set C
        i = 24
        while i <= 31:
            parametros.append(p[i])
            i += 1
    return parametros

def correr_ensayos_experimento(parametros):
    '''
    Corre ensayos en pantalla de manera aleatoria. Además, cuando se está a la mitad de la prueba, se muestra una pantalla que indica esta condición.
    '''
    pruebas = list(range(24))
    i = 1

    while pruebas != []:
        # Mitad del experimento
        if i == 12:				
            pygame.mouse.set_visible(0)
            gbl.screen.fill(ctes.WHITE)
            text1 = gbl.font25.render(ctes.MITAD_PRUEBA_MSG,True,(0,0,0))
            text1_width = text1.get_width()
            gbl.screen.blit(text1,[gbl.mx-text1_width/2,gbl.my])
            done = False
            while not done:
                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_SPACE:
                            done = True
                pygame.display.flip()
        prueba = choice(pruebas)
        pruebas.remove(prueba)

        w = ensayos.ensayos(parametros[prueba])
        demanda = int(parametros[prueba][1][0])

        datos_escritor = gbl.SUJETO+','+gbl.NOMBRE+','+gbl.F_D_N+','+gbl.EDAD+','+gbl.F_D_E+','+gbl.SEXO+','+gbl.GRADO+','+gbl.EVALUADOR+','+gbl.FORMA+','+'Act'+','+str(parametros[prueba][0])+','+parametros[prueba][2]+','+parametros[prueba][1]+','+str(parametros[prueba][3])
        escritor.escribir(demanda,w,datos_escritor)
        i += 1

def guardar_tiempo_total():
    '''
    Guarda tiempo total de la prueba al final del archivo de resultados del participante.
    '''
    tiempototal = round(gbl.clock_absoluto.tick()/60000.0,2)
    # gbl.archivo.seek(-2, os.SEEK_END)
    gbl.archivo.seek(0, os.SEEK_END)
    gbl.archivo.seek(gbl.archivo.tell() -2, os.SEEK_SET)
    gbl.archivo.truncate()
    gbl.archivo.write(","+str(tiempototal) + ' min')
    gbl.archivo.close()


def imprimir_pantalla_final():
    '''
    Imprime pantalla final del experimento.
    '''
    pygame.mouse.set_visible(0)
    gbl.screen.fill(ctes.WHITE)
    text1 = gbl.font25.render(ctes.GRACIAS_MSG2,True,(0,0,0))
    text1_width = text1.get_width()
    gbl.screen.blit(text1,[gbl.mx-text1_width/2,gbl.my])
    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    done = True
        pygame.display.flip()
    pygame.quit()