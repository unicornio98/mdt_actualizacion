#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""constantes.py:
   Este archivo contiene todas las constantes utilizadas a lo largo del 
   proyecto. Estas incluyen constantes generales, de configuración, paths de 
   archivos a leer, mensajes a imprimir y diccionarios de utilidad.
"""
# =============================================================================
# Generales
# =============================================================================

WHITE = (255,255,255)
BLUE = (51,51,255)

# =============================================================================
# Configuación
# =============================================================================

CORRER_DATOS_SUJETOS = True
CORRER_INSTRUCCIONES = True
CORRER_PRACTICA = True
CORRER_EXPERIMENTO = True
CORRER_CAJA_COMENTARIOS = True

datos_participante = {
    "nombre": 'PRUEBA',
    "fdn": 'PRUEBA',
    "sexo": 'PRUEBA',
    "grado": 'PRUEBA',
    "sujeto": '999',
    "forma": 'PRUEBA',
    "destroy": 0,
    "evaluador": 'PRUEBA',
    "proyecto": 'PRUEBA',
    "fde":'PRUEBA',
    "edad": 'PRUEBA',
}

# =============================================================================
# Paths
# =============================================================================

# CSV PATHS
INSTR_PATH = './recursos/archivos/instrucciones.csv'
PRACTICE_PATH = './recursos/archivos/practica.csv'
DIR_PRACTICE_PATH = './recursos/archivos/practica_dirigida.csv'
ENSAYOS1_PATH = './generador/ensayos/ensayos-1.csv'

# MEDIA PATHS
NEG_SND_PATH = "./recursos/media/error.wav"
POS_SND_PATH = "./recursos/media/coin.wav"
PUNTO_FIJ_PATH = './recursos/media/PF.png'
BOLA_VERDE_PATH = './recursos/media/BV.png'
PREGUNTA_PATH =  './recursos/media/pregunta.png'
CHECK_PATH = './recursos/media/check.png'
EQUIS_PATH = './recursos/media/equis.png'
IGUAL_PATH = './recursos/media/igual.png'
BV_PATH = './recursos/media/BV.png'

# OTROS PATHS
COMENT_PATH = '../evaluaciones/Actualizacion--comentarios--sujeto-'
EVALUACIONES_PATH = '../evaluaciones'
RESULT_PARTICIPANTE_PATH = '../evaluaciones/Actualizacion--sujeto-' 

# GENERADOR
FLECHAS_PATH = './generador/flechas/'
ESTIMULOS_PATH = './generador/estimulos/'

# =============================================================================
# Mensajes
# =============================================================================

INICIAR_MSG = '¡Ahora vas a iniciar la práctica!'
BARRA_ESP_MSG = 'Presioná la barra espaciadora para empezar'
LLAMAR_INV_MSG = 'Por favor llamá al investigador.'
BARRA_ESP_PR_MSG = 'Presioná la barra espaciadora para empezar la práctica'
GRACIAS_MSG = '¡Muchas gracias por participar!'
FIN_MSG = 'Fin de la práctica.'
BARRA_ESP_INI = '¡Presioná la barra espaciadora para iniciar el juego!'
MITAD_PRUEBA_MSG = "Mitad de la prueba. Presione espacio para continuar."
GRACIAS_MSG2 = '¡Muchas Gracias!'
SALIR_PRUEBA_MSG = '¿Está seguro de que desea salir y terminar la prueba? (S/N)'
PREGUNTA_FIG_MSG = '¿Dónde estaba esta figura?'
CLIC_FIG = 'Da click donde estaba la figura.'
BARRA_ESP_CONT = 'Presioná la barra espaciadora para continuar.'
INSTR_MSG = 'INSTRUCCIONES'

# =============================================================================
# Diccionarios
# =============================================================================
'''
	Diccionario que identifica la flecha en función del cuadro donde se debe dar clic, por lo tanto se asigna a cada casilla una flecha. 
	Devuelve un string, con el nombre de la flecha 
	'''
BANCO_FLECHAS = {
		1: 'FNOV.png',
		3: 'FNV.png',
		5: 'FNEV.png',
		7: 'FNON.png',
		8: 'FNN.png',
		9: 'FNEN.png',
		11: 'FOV.png',
		12: 'FON.png',
		14: 'FEN.png',
		15: 'FEV.png',
		17: 'FSON.png',
		18: 'FSN.png',
		19: 'FSEN.png',
		21: 'FSOV.png',
		23: 'FSV.png',
		25: 'FSEV.png'
    }


'''
	Diccionario que dado el nombre de la flecha devuelve el numero de la casilla a la que esta asiganda, o haciendo referencia.
	'''
BANCO_POSICIONES = {
		'FNOV.png': 1,
		'FNV.png': 3,
		'FNEV.png': 5,
		'FNON.png': 7,
		'FNN.png': 8,
		'FNEN.png': 9,
		'FOV.png': 11,
		'FON.png': 12,
		'FEN.png': 14,
		'FEV.png': 15,
		'FSON.png': 17,
		'FSN.png': 18,
		'FSEN.png': 19,
		'FSOV.png': 21,
		'FSV.png': 23,
		'FSEV.png': 25
	}
