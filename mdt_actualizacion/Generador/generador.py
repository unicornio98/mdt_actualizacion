#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""generador.py:
   Este archivo contiene funciones que permiten generar los ensayos. Archivo 
   principal a ejecutar.
"""
# =============================================================================
# Imports
# =============================================================================
import sys
sys.dont_write_bytecode = True
import os
from ensayo import gen_ensayo 
from escritor_ensayos import escritor_ensayos
from math import sqrt
# =============================================================================
# Métodos: generales
# =============================================================================

#Le pregunta al usuario cuantos archivos de ensayos desea generar
n = int(input("Numero de ensayos a generar: "))

estimulos = []
#Crea el directorio generador/ensayos si no existe aun.
try:
	test = open(os.path.dirname(os.path.realpath(__file__)) +"\\"+'generador.py')
	test.close()
	if not os.path.exists(os.path.dirname(os.path.realpath(__file__)) +"\\"+'ensayos'):
		os.makedirs('ensayos')
	#determina cuantos archivos hay dentro del directorio para darle nuevos nombres a los archivos de ensayos si se vuelve a correr el generador y que no sobreescriba los que ya existen.
	n_i = len(os.listdir(os.path.dirname(os.path.realpath(__file__)) +"\\"+'ensayos'))/2

	#crea una lista con los nombres de todos los estimulos, que los saca de los nombres de los archivos de imagenes en el directorio de estimulos.
	for i in [f for f in os.listdir(os.path.dirname(os.path.realpath(__file__)) +"\\"+'estimulos') if f[0] != '.' and f[-4:] == '.png']:
		estimulos.append(i)
	dirc = os.path.dirname(os.path.realpath(__file__)) +"\\"+'ensayos'+ "\\"

except:
	if not os.path.exists(os.path.dirname(os.path.realpath(__file__)) +"/"+'ensayos'):
		os.makedirs('ensayos')
	n_i = len(os.listdir(os.path.dirname(os.path.realpath(__file__)) +"/"+'ensayos'))/2

	for i in [f for f in os.listdir(os.path.dirname(os.path.realpath(__file__)) +"/"+'estimulos') if f[0] != '.' and f[-4:] == '.png']:
		estimulos.append(i)
	dirc = os.path.dirname(os.path.realpath(__file__)) +"/"+'ensayos'+ "/"
 
#Se crea uno de los archivos de ensayos por cada vuelta por este ciclo:
i = 1 
while  i <= n:
	
	#Estructura del anclaje y los sets A, B, C.
	#El numero determina la demanda y el a o b determina si es carga alta o baja.
	pruebas_anclaje= ['2a','2a','2b','2b','3a','3a','3b','3b']
	pruebas_setA = ['2a','2a','2b','2b','3a','3a','3b','3b']
	pruebas_setB = ['2a','2a','2b','2b','3a','3a','3b','3b']
	pruebas_setC = ['2a','2a','2b','2b','3a','3a','3b','3b']
	pruebas = pruebas_anclaje + pruebas_setA + pruebas_setB + pruebas_setC
	ensayos = []
	
	j=1
	#Para cada entrada especificada genera uno de los ensayos y lo guarda en la lista 'ensayos'
	for prueba in pruebas:
		ensayos.append(gen_ensayo(prueba,estimulos,j))	
		j+=1
		
	'''
	En lo siguiente se calacula el promedio y la desviacion estandar de los valores de 'interferencia' para las distintas formas.
	Estos valores se guardan en un archivo aparte.
	'''
	p_1 = 0
	p2_1 = 0
	p_2 = 0
	p2_2 = 0
	p_3 = 0
	p2_3 = 0
	#Forma 1
	for e in ensayos[0:27]:
		p_1 += e[9]
	for e in ensayos[0:27]:
		p2_1 += e[9]**2
	#Forma 2
	for e in ensayos[0:11]:
		p_2 += e[9]
	for e in ensayos[0:11]:
		p2_2 += e[9]**2
	for e in ensayos[20:35]:
		p_2 += e[9]
	for e in ensayos[20:35]:
		p2_2 += e[9]**2
	#Forma 3
	for e in ensayos[0:19]:
		p_3 += e[9]
	for e in ensayos[0:19]:
		p2_3 += e[9]**2
	for e in ensayos[28:35]:
		p_3 += e[9]
	for e in ensayos[28:35]:
		p2_3 += e[9]**2
	
	p_1 = float(p_1)/28.
	p2_1 = float(p2_1)/28.
	d_1 = sqrt(p2_1 - p_1**2)
	
	p_2 = float(p_2)/28.
	p2_2 = float(p2_2)/28.
	d_2 = sqrt(p2_2 - p_2**2)
	
	p_3 = float(p_3)/28.
	p2_3 = float(p2_3)/28.
	d_3 = sqrt(p2_3 - p_3**2)
	
	
	print('promedio F1: ' + str(p_1))
	print('desviacion F1: '+ str(d_1))
	
	print('promedio F2: ' + str(p_2))
	print('desviacion F2: '+ str(d_2))
	
	print('promedio F3: ' + str(p_3))
	print('desviacion F3: '+ str(d_3))
	
	interferencia_pd = [[p_1,d_1],[p_2,d_2],[p_3,d_3]]
	
	#Toma los ensayos generados y los valores de interferencia y escribe los archivos
	escritor_ensayos(n_i+i, dirc, ensayos, interferencia_pd)
	i += 1




		
		
