#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""escritor_ensayos.py:
   Se encarga de escribir los ensayos generados en un archivo csv.
"""
# =============================================================================
# Métodos: generales
# =============================================================================
def escritor_ensayos(num, dir, ensayos, interferencia_pd):
	'''
	Recibe la informacion de los ensayos y escribe un archivo csv.
	Para cada ensayo, los indices de la lista se identifican con
	0: demanda
	1: ESTIMULOS
	2: POS_ESTIMULOS
	3: POSICIONES_BOLA
	4: POSICIONES_CAMBIO
	5: FLECHAS
	6: CARGA
	7: SET
	8: INTERFERENCIA
	9: INTERFERENCIA_TOTAL
	'''
	
	file = open(dir + 'ensayos-' + str(num) + '.csv', 'w')
	file.write('Generado ' + str(num)+ '\n')
	file.write(','+'DEMANDA'+','+'SET'+','+'CARGA'+','
		+'n_EC1'+','+'PEC_1'+','+'PEA1_1'+',' + 'PCA1_1'+ ','+ 'PEA1_2' + ',' + 'PCA1_2' + ',' +  'PEA1_3' + ',' + 'PCA1_3'
		+ ',' + 'F1_1'+','+'F2_1'+','+'F3_1'+','
		+'n_EC2'+','+'PEC_2'+','+'PEA2_1'+',' + 'PCA2_1'+ ','+ 'PEA2_2' + ',' + 'PCA2_2'  + ',' +  'PEA2_3' + ',' + 'PCA2_3'
		+ ',' + 'F1_2'+','+'F2_2'+','+'F3_2'+','
		+'n_EC3'+','+'PEC_3'+','+'PEA3_1'+',' + 'PCA3_1'+ ','+ 'PEA3_2' + ',' + 'PCA3_2'  + ',' + 'PEA3_3' + ',' + 'PCA3_3' 
		+ ',' + 'F1_3'+','+'F2_3'+','+'F3_3'+','
		+'n_EC4'+','+'PEC_4'+','+'PEA4_1'+',' + 'PCA4_1'+ ','+ 'PEA4_2' + ',' + 'PCA4_2' + ','+ 'PEA4_3' + ',' + 'PCA4_3' 
		+ ',' + 'F1_4'+','+'F2_4'+','+'F3_4'+',INTERFERENCIA_E1,INTERFERENCIA_E2,INTERFERENCIA_E3,INTERFERENCIA_E4,INTERFERENCIA_TOTAL'
		+'\n')
	i = 1
	for ens in ensayos:
		file.write(str(i) + ',' + ens[0]+','+ens[7]+','+ str(ens[6])+','
			+ens[1][0]+','+str(ens[2][0])+','+ens[3][0][0]+','+ens[4][0][0]+','+ens[3][0][1]+','+ens[4][0][1]+','+ens[3][0][2]+','+ens[4][0][2]+','
			+ens[5][0][0]+','+ens[5][0][1]+','+ens[5][0][2]+','
			+ens[1][1]+','+ str(ens[2][1])+','+ens[3][1][0]+','+ens[4][1][0]+','+ens[3][1][1]+','+ens[4][1][1]+','+ens[3][1][2]+','+ens[4][1][2]+','
			+ens[5][1][0]+','+ens[5][1][1]+','+ens[5][1][2]+','
			+ens[1][2]+','+ str(ens[2][2])+','+ens[3][2][0]+','+ens[4][2][0]+','+ens[3][2][1]+','+ens[4][2][1]+','+ens[3][2][2]+','+ens[4][2][2]+','
			+ens[5][2][0]+','+ens[5][2][1]+','+ens[5][2][2]+','
			+ens[1][3]+','+ str(ens[2][3])+','+ens[3][3][0]+','+ens[4][3][0]+','+ens[3][3][1]+','+ens[4][3][1]+','+ens[3][3][2]+','+ens[4][3][2]+','
			+ens[5][3][0]+','+ens[5][3][1]+','+ens[5][3][2]+','
			+str(ens[8][0])+','+str(ens[8][1])+','+str(ens[8][2])+','+str(ens[8][3])+','+str(ens[9])+','
			+'\n')
		i += 1
	file.close()	
	
	file = open(dir + 'ensayos-' + str(num) + '_interferencia.csv', 'w')
	file.write(',PROMEDIO,DESVIACION ESTANDAR\n')
	file.write('FORMULA 1,'+str(interferencia_pd[0][0])+','+str(interferencia_pd[0][1])+'\n')
	file.write('FORMULA 2,'+str(interferencia_pd[1][0])+','+str(interferencia_pd[1][1])+'\n')
	file.write('FORMULA 3,'+str(interferencia_pd[2][0])+','+str(interferencia_pd[2][1])+'\n')
	file.close()