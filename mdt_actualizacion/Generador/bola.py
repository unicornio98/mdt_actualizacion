#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""bola.py:
   Genera posiciones para el objeto bola.
"""
# =============================================================================
# Imports
# =============================================================================
from random import choice
from coor_casillas import matriz_casilla
# =============================================================================
# Métodos: generales
# =============================================================================

def cambio_posicion(posicion_act, cuadros, casilla_est):
	'''
	Determina un cambio de posición de la bola dada la posicion actual y los cuadros a los que se le permite moverse.
	Entradas: 
		posicion_act: son las 'coordenadas' de la casilla actual de la bola, dadas como fila y columna dentro de la grilla (por ejemplo, [1,3] es la casilla 3, [2,5] es la casilla 10, etc)
		cuadros: es la lista de cuadros que no estan deshabilitados para la aparición de la bola
		casilla_est: es la lista de las casillas en las que se encuentran los estimulos por codificar
	'''

	x_act, y_act = posicion_act[0], posicion_act[1]
	
	while True:
		#si esta en la primera fila, la bola solo puede o mantenerse en esa fila o bajar.
		if x_act == 1:
			suma = choice([0,1])
			x = x_act + suma
		#si esta en la ultima fila, la bola solo puede o mantenerse en esa fila o subir
		elif x_act == 5:
			suma = choice([-1,0])
			x = x_act + suma
		#Si no esta ni en la primera ni en la ultima fila, puede mantenerse en la misma, subir o bajar
		else:
			suma = choice([-1,0,1])
			x = x_act + suma

		#Si se mantiene en la misma fila, obliga a que haya un movimiento de columna
		if suma == 0:
			#si está en la primera columna, debe movierse a la derecha
			if y_act == 1:
				y = y_act + 1
			#si esta en la ultima columna, debe movierse a la izquierda
			elif y_act == 5:
				y = y_act - 1
			#si esta en otra columna, puede moverse para la derecha o para la izquierda
			else:
				suma = choice([-1,1])
				y = y_act + suma
		#si hubo movimiento de fila, tiene la posibilidad de mantenerse en la misma columna
		else:
			#si esta en la primera columna, puede mantenerse en ella o moverse a la derecha
			if y_act == 1:
				suma = choice([0,1])
				y = y_act + suma
			#si está en la ultima columna, puede mantenerse en ella o moverse a la izquierda
			elif y_act == 5:
				suma = choice([-1,0])
				y = y_act + suma
			#si no esta ni en la primera ni en la ultima columna, puede mantenerse en la que está o moverse para la derecha o izquierda
			else:
				suma = choice([-1,0,1])
				y = y_act + suma
		
		#comprueba que la casilla que escogió esté habilitada para mostrar la bola. Si es así, rompe el ciclo, si no, vuelve a correr el algoritmo de escogencia de casilla nueva.
		#se repite hasta que se cumpla que se escogió una casilla habilitada
		if matriz_casilla((x,y)) in cuadros:
			if str(matriz_casilla((x,y))) not in casilla_est:
				break

	#devuelve la fila y columna de la posicion a la que se mueve la bola.
	return (x,y)


