#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""escoger_flecha.py:
   Escoge la flecha en función de la posición anterior y la nueva.
"""
# =============================================================================
# Imports
# =============================================================================
from coor_casillas import matriz_casilla
# =============================================================================
# Métodos: generales
# =============================================================================

def escoger_flecha(PA, PN):	
	'''
	Escoge la flecha en funcion de la posicion anterior y la nueva.
	'''
	if PA < PN:
		if PN - PA == 1:
			flecha = 'FEN.png'
		elif PN - PA == 4:
			flecha = 'FSON.png'
		elif PN - PA == 5:
			flecha = 'FSN.png'
		elif PN - PA == 6:
			flecha = 'FSEN.png'	
	else:
		if PA - PN == 1:
			flecha = 'FON.png'
		elif PA - PN == 4:
			flecha = 'FNEN.png'
		elif PA - PN == 5:
			flecha = 'FNN.png'
		elif PA - PN == 6:
			flecha = 'FNON.png'	
	return flecha

