#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""ensayo.py:
   Genera un ensayo.
"""
# =============================================================================
# Imports
# =============================================================================
from escoger_estimulos import escoger_estimulos
from escoger_flecha import escoger_flecha
from random import choice
from coor_casillas import matriz, matriz_casilla, casillas_vecinas
from bola import cambio_posicion 
# =============================================================================
# Métodos: generales
# =============================================================================

def gen_ensayo (demanda, estimulos,j):
	'''
	Genera un ensayo.
	Le entra:
	demanda: contiene en realidad la información de la demanda y la carga en forma de un string de la forma 'numero''letra' (ejemplo 2a), donde el numero es la demanda y la letra es la carga
	estimulos: la lista de estimulos
	j: el numero de ensayo que se está generando
	
	Devuelve una lista con toda la informacion del ensayo.
		0: demanda
		1: ESTIMULOS
		2: POS_ESTIMULOS
		3: POSICIONES_BOLA
		4: POSICIONES_CAMBIO
		5: FLECHAS
		6: CARGA
		7: SET
		8: INTERFERENCIA
		9: INTERFERENCIA_TOTAL
	'''
	demanda,carga = demanda
	
	#dependiendo del numero de ensayo que se está generando, lo identifica como set de anclaje o set A, B o C
	if j <= 8:
		SET = 'ANC'
	elif j > 8 and j <= 16:
		SET = 'A'
	elif j > 16 and j <= 24:
		SET = 'B'
	else:
		SET = 'C'
	
	#identifica la carga como alta o baja
	if carga == 'a':
		# CARGA ALTA 
		CARGA = 0.6
	else:
		#CARGA BAJA
		CARGA = 1.6

	#escoge los estimulos a codificar para el ensayo
	escogidos = escoger_estimulos(demanda, estimulos)
	#crea distintas listas que contienen las casillas totales
	cuadros = list(range(1,26))
	cuadros.remove(13)
	cuadros_salida = list(range(1,26))
	cuadros_est = list(range(1,26))
	cuadros_est.remove(13)
	
	#Crea las listas que van a contener la información del ensayo
	ESTIMULOS = ['null','null','null','null'] 
	POS_ESTIMULOS = ['null','null','null','null']
	POSICIONES_BOLA = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	POSICIONES_CAMBIO = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	FLECHAS = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	INTERFERENCIA = ['null','null','null','null']
	
	#Coloca los primeros dos estimulos escogidos en la lista ESTIMULOS (dado que la demanda minima es 2, todo ensayo cotniene por lo menos estos dos estimulos por codificar)
	ESTIMULOS[0] = escogidos[0]
	ESTIMULOS[1] = escogidos[1]
	
	#escoge la posicion del primer estimulo a codificar
	posicion_E_1 = choice(cuadros_est)
	#si la casilla existe en cuadros_salida, la elimina de esta lista
	try:
		cuadros_salida.remove(posicion_E_1)
	except:
		pass
	#borra la casilla de la lista de casillas para estimulos
	cuadros_est.remove(posicion_E_1)
	POS_ESTIMULOS[0] = str(posicion_E_1)
	
	#determina las casillas vecinas del estimulo a codificar 1
	vecinas_E1_p_2, vecinas_E1_p_1 = casillas_vecinas(posicion_E_1)
	interferencia_E_1 = 0

	#escoge la casilla de salida de la bola
	add1 = False
	casilla_B1 = choice(cuadros_salida)
	cuadros_salida.remove(casilla_B1)
	coord_B1_1 = matriz(casilla_B1)
	#determina el cambio de posicion de la bola
	coord_B1_2 = cambio_posicion(coord_B1_1, cuadros,POS_ESTIMULOS)
	#si la casilla está en la lista de cuadros de salida, la elimina y define la variable add1 como verdadera para volverla a agregar más adelante.
	if matriz_casilla(coord_B1_2) in cuadros_salida:
		cuadros_salida.remove(matriz_casilla(coord_B1_2))
		add1 = True
	
	#Mismo procedimiento para la segunda bola (como se elimino la posicion de cambio de la primera de la lista de salida, no puede salir de la casilla a la que se movio la primera bola)
	add2 = False
	casilla_B2 = choice(cuadros_salida)
	cuadros_salida.remove(casilla_B2)
	coord_B2_1 = matriz(casilla_B2)
	coord_B2_2 = cambio_posicion(coord_B2_1, cuadros,POS_ESTIMULOS)
	if matriz_casilla(coord_B2_2) in cuadros_salida:
		cuadros_salida.remove(matriz_casilla(coord_B2_2))
		add2 = True
	#Agrega de nuevo la casilla de salida a la lista de salida si add1 es verdadera, habilitando esa posicion de salida para la siguiente bola
	if add1:
		cuadros_salida.append(matriz_casilla(coord_B1_2))
	
	#Mismo procedimeinto para la tercera bola
	casilla_B3 = choice(cuadros_salida)
	cuadros_salida.remove(casilla_B3)
	coord_B3_1 = matriz(casilla_B3)
	coord_B3_2 = cambio_posicion(coord_B3_1, cuadros,POS_ESTIMULOS)
	if add2:
		cuadros_salida.append(matriz_casilla(coord_B2_2))
	
	#Guardas las posiciones de salida y de cambio 
	POSICIONES_BOLA[0] = [matriz_casilla(coord_B1_1), matriz_casilla(coord_B2_1), matriz_casilla(coord_B3_1)]
	POSICIONES_CAMBIO[0] = [matriz_casilla(coord_B1_2), matriz_casilla(coord_B2_2), matriz_casilla(coord_B3_2)]
	
	#Suma los valores de interferencia para el estimulo a codificar 1
	#Si la bola sale en una casilla inmediata, le suma 2 a la interferencia
	#Si la bola sale en una casilla que esta a una casilla de distancia de la casilla del estimulo, le suma 1 a la interferencia
	for b in POSICIONES_BOLA[0]:
		if b in vecinas_E1_p_2:
			interferencia_E_1 += 2
		if b in vecinas_E1_p_1:
			interferencia_E_1 += 1			
	for b in POSICIONES_CAMBIO[0]:
		if b in vecinas_E1_p_2:
			interferencia_E_1 += 2
		if b in vecinas_E1_p_1:
			interferencia_E_1 += 1			

	#dadas las posiciones iniciales y de cambio de las bolas, identifica las flechas correspondientes
	for i in range(3):
		FLECHAS[0][i] = escoger_flecha(POSICIONES_BOLA[0][i],POSICIONES_CAMBIO[0][i])


	#En lo que sigue, se repite el mismo procedimeinto para el segundo estimulo a codificar y su respectiva fase de eventos
	posicion_E_2 = choice(cuadros_est)
	try:
		cuadros_salida.remove(posicion_E_2)
	except:
		pass
	cuadros_est.remove(posicion_E_2)
	POS_ESTIMULOS[1] = str(posicion_E_2)
	
	vecinas_E2_p_2, vecinas_E2_p_1 = casillas_vecinas(posicion_E_2)
	interferencia_E_2 = 0
	
	add1 = False
	casilla_B1 = choice(cuadros_salida)
	cuadros_salida.remove(casilla_B1)
	coord_B1_1 = matriz(casilla_B1)
	coord_B1_2 = cambio_posicion(coord_B1_1, cuadros,POS_ESTIMULOS)
	if matriz_casilla(coord_B1_2) in cuadros_salida:
		cuadros_salida.remove(matriz_casilla(coord_B1_2))
		add = True
	
	add2 = False
	casilla_B2 = choice(cuadros_salida)
	cuadros_salida.remove(casilla_B2)
	coord_B2_1 = matriz(casilla_B2)
	coord_B2_2 = cambio_posicion(coord_B2_1, cuadros,POS_ESTIMULOS)
	if matriz_casilla(coord_B2_2) in cuadros_salida:
		cuadros_salida.remove(matriz_casilla(coord_B2_2))
		add2 = True
	if add1:
		cuadros_salida.append(matriz_casilla(coord_B1_2))
	
	casilla_B3 = choice(cuadros_salida)
	cuadros_salida.remove(casilla_B3)
	coord_B3_1 = matriz(casilla_B3)
	coord_B3_2 = cambio_posicion(coord_B3_1, cuadros,POS_ESTIMULOS)
	if add2:
		cuadros_salida.append(matriz_casilla(coord_B2_2))
	
	POSICIONES_BOLA[1] = [matriz_casilla(coord_B1_1), matriz_casilla(coord_B2_1), matriz_casilla(coord_B3_1)]
	POSICIONES_CAMBIO[1] = [matriz_casilla(coord_B1_2), matriz_casilla(coord_B2_2), matriz_casilla(coord_B3_2)]
	
	for i in range(3):
		FLECHAS[1][i] = escoger_flecha(POSICIONES_BOLA[1][i],POSICIONES_CAMBIO[1][i])
		
	for b in POSICIONES_BOLA[1]:
		if b in vecinas_E1_p_2:
			interferencia_E_1 += 2
		if b in vecinas_E1_p_1:
			interferencia_E_1 += 1
		if b in vecinas_E2_p_2:
			interferencia_E_2 += 2
		if b in vecinas_E2_p_1:
			interferencia_E_2 += 1
	for b in POSICIONES_CAMBIO[1]:
		if b in vecinas_E1_p_2:
			interferencia_E_1 += 2
		if b in vecinas_E1_p_1:
			interferencia_E_1 += 1
		if b in vecinas_E2_p_2:
			interferencia_E_2 += 2
		if b in vecinas_E2_p_1:
			interferencia_E_2 += 1
			
	#Si la demanda fuera 3 o 4 (demanda 4 no se esta usando), se repite lo anterior para estos estimulos
	if demanda == '3' or demanda == '4':
		ESTIMULOS[2] = escogidos[2]


		posicion_E_3 = choice(cuadros_est)
		try:
			cuadros_salida.remove(posicion_E_3)
		except:
			pass
		cuadros_est.remove(posicion_E_3)
		POS_ESTIMULOS[2] = str(posicion_E_3)
		
		vecinas_E3_p_2, vecinas_E3_p_1 = casillas_vecinas(posicion_E_3)
		interferencia_E_3 = 0
		
		add1 = False
		casilla_B1 = choice(cuadros_salida)
		cuadros_salida.remove(casilla_B1)	
		coord_B1_1 = matriz(casilla_B1)
		coord_B1_2 = cambio_posicion(coord_B1_1, cuadros,POS_ESTIMULOS)
		if matriz_casilla(coord_B1_2) in cuadros_salida:
			cuadros_salida.remove(matriz_casilla(coord_B1_2))
	
		add2= False
		casilla_B2 = choice(cuadros_salida)
		cuadros_salida.remove(casilla_B2)
		coord_B2_1 = matriz(casilla_B2)
		coord_B2_2 = cambio_posicion(coord_B2_1, cuadros,POS_ESTIMULOS)
		if matriz_casilla(coord_B2_2) in cuadros_salida:
			cuadros_salida.remove(matriz_casilla(coord_B2_2))
			add2 = True
		if add1:
			cuadros_salida.append(matriz_casilla(coord_B1_2))
	
		casilla_B3 = choice(cuadros_salida)
		cuadros_salida.remove(casilla_B3)
		coord_B3_1 = matriz(casilla_B3)
		coord_B3_2 = cambio_posicion(coord_B3_1, cuadros,POS_ESTIMULOS)
		if add2:
			cuadros_salida.append(matriz_casilla(coord_B2_2))
	
		POSICIONES_BOLA[2] = [matriz_casilla(coord_B1_1), matriz_casilla(coord_B2_1), matriz_casilla(coord_B3_1)]
		POSICIONES_CAMBIO[2] = [matriz_casilla(coord_B1_2), matriz_casilla(coord_B2_2), matriz_casilla(coord_B3_2)]
		
		for b in POSICIONES_BOLA[2]:
			if b in vecinas_E1_p_2:
				interferencia_E_1 += 2
			if b in vecinas_E1_p_1:
				interferencia_E_1 += 1
			if b in vecinas_E2_p_2:
				interferencia_E_2 += 2
			if b in vecinas_E2_p_1:
				interferencia_E_2 += 1
			if b in vecinas_E3_p_2:
				interferencia_E_3 += 2
			if b in vecinas_E3_p_1:
				interferencia_E_3 += 1
		for b in POSICIONES_CAMBIO[2]:
			if b in vecinas_E1_p_2:
				interferencia_E_1 += 2
			if b in vecinas_E1_p_1:
				interferencia_E_1 += 1
			if b in vecinas_E2_p_2:
				interferencia_E_2 += 2
			if b in vecinas_E2_p_1:
				interferencia_E_2 += 1
			if b in vecinas_E3_p_2:
				interferencia_E_3 += 2
			if b in vecinas_E3_p_1:
				interferencia_E_3 += 1

		for i in range(3):
			FLECHAS[2][i] = escoger_flecha(POSICIONES_BOLA[2][i],POSICIONES_CAMBIO[2][i])
			
		if demanda == '4':
			ESTIMULOS[3] = escogidos[3]


			posicion_E_4 = choice(cuadros_est)
			try:
				cuadros_salida.remove(posicion_E_4)
			except:
				pass
			cuadros_est.remove(posicion_E_4)
			POS_ESTIMULOS[3] = str(posicion_E_4)
			
			vecinas_E4_p_2, vecinas_E4_p_1 = casillas_vecinas(posicion_E_4)
			interferencia_E_4 = 0
			
			add1 = True
			casilla_B1 = choice(cuadros_salida)
			cuadros_salida.remove(casilla_B1)	
			coord_B1_1 = matriz(casilla_B1)
			coord_B1_2 = cambio_posicion(coord_B1_1, cuadros,POS_ESTIMULOS)
			if matriz_casilla(coord_B1_2) in cuadros_salida:
				cuadros_salida.remove(matriz_casilla(coord_B1_2))
				add1 = True
	
			add2 = False
			casilla_B2 = choice(cuadros_salida)
			cuadros_salida.remove(casilla_B2)
			coord_B2_1 = matriz(casilla_B2)
			coord_B2_2 = cambio_posicion(coord_B2_1, cuadros,POS_ESTIMULOS)
			if matriz_casilla(coord_B2_2) in cuadros_salida:
				cuadros_salida.remove(matriz_casilla(coord_B2_2))
				add2 = True
			if add2:
				cuadros_salida.append(matriz_casilla(coord_B1_2))
	
			casilla_B3 = choice(cuadros_salida)
			cuadros_salida.remove(casilla_B3)
			coord_B3_1 = matriz(casilla_B3)
			coord_B3_2 = cambio_posicion(coord_B3_1, cuadros,POS_ESTIMULOS)
			if add2:
				cuadros_salida.append(matriz_casilla(coord_B2_2))
	
			POSICIONES_BOLA[3] = [matriz_casilla(coord_B1_1), matriz_casilla(coord_B2_1), matriz_casilla(coord_B3_1)]
			POSICIONES_CAMBIO[3] = [matriz_casilla(coord_B1_2), matriz_casilla(coord_B2_2), matriz_casilla(coord_B3_2)]
			
			
			for b in POSICIONES_BOLA[3]:
				if b in vecinas_E1_p_2:
					interferencia_E_1 += 2
				if b in vecinas_E1_p_1:
					interferencia_E_1 += 1
				if b in vecinas_E2_p_2:
					interferencia_E_2 += 2
				if b in vecinas_E2_p_1:
					interferencia_E_2 += 1
				if b in vecinas_E3_p_2:
					interferencia_E_3 += 2
				if b in vecinas_E3_p_1:
					interferencia_E_3 += 1
				if b in vecinas_E4_p_2:
					interferencia_E_4 += 2
				if b in vecinas_E4_p_1:
					interferencia_E_4 += 1
			for b in POSICIONES_CAMBIO[3]:
				if b in vecinas_E1_p_2:
					interferencia_E_1 += 2
				if b in vecinas_E1_p_1:
					interferencia_E_1 += 1
				if b in vecinas_E2_p_2:
					interferencia_E_2 += 2
				if b in vecinas_E2_p_1:
					interferencia_E_2 += 1
				if b in vecinas_E3_p_2:
					interferencia_E_3 += 2
				if b in vecinas_E3_p_1:
					interferencia_E_3 += 1
				if b in vecinas_E4_p_2:
					interferencia_E_4 += 2
				if b in vecinas_E4_p_1:
					interferencia_E_4 += 1
			
			for i in range(3):
				FLECHAS[3][i] = escoger_flecha(POSICIONES_BOLA[3][i],POSICIONES_CAMBIO[3][i])

	for i in range(4):
		for j in range(3):
			POSICIONES_BOLA[i][j] = str(POSICIONES_BOLA[i][j])
			POSICIONES_CAMBIO[i][j] = str(POSICIONES_CAMBIO[i][j])
	
	#Se guardan los valores finales de las interferencias de los estimulos
	INTERFERENCIA[0] = interferencia_E_1
	INTERFERENCIA[1] = interferencia_E_2
	if demanda == '3' or demanda == '4':
		INTERFERENCIA[2] = interferencia_E_3
		if demanda == '4':
			INTERFERENCIA[3] = interferencia_E_4
			
	
	#Da la interferencia total
	INTERFERENCIA_TOTAL = interferencia_E_1 + interferencia_E_2
	if demanda == '3' or demanda == '4':
		INTERFERENCIA_TOTAL += interferencia_E_3
		if demanda == '4':
			INTERFERENCIA_TOTAL += interferencia_E_4
	
	
	#Devuelve una lista con la informacion del ensayo
	return [demanda, ESTIMULOS, POS_ESTIMULOS, POSICIONES_BOLA,POSICIONES_CAMBIO, FLECHAS, CARGA, SET, INTERFERENCIA, INTERFERENCIA_TOTAL]