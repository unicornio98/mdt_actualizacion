#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""coor_casilllas.py:
   Genera representación matricial [x,y] de las posiciones en la grilla (1-25).
"""
# =============================================================================
# Métodos: generales
# =============================================================================
def matriz(n):
	'''
	Pasa de la representacion de posiciones en la grilla dada por el numero n = 1-25 a la representación matricial [x,y] con x la fila (1-5) e y la columna (1-5)
	'''
	if n == 1:
		c = (1,1)
	elif n == 2:
		c = (1,2)
	elif n == 3:
		c = (1,3)
	elif n == 4:
		c = (1,4)
	elif n == 5:
		c = (1,5)

	elif n == 6:
		c = (2,1)
	elif n == 7:
		c = (2,2)
	elif n == 8:
		c = (2,3)
	elif n == 9:
		c = (2,4)
	elif n == 10:
		c = (2,5)

	elif n == 11:
		c = (3,1)
	elif n == 12:
		c = (3,2)
	elif n == 13:
		c = (3,3)
	elif n == 14:
		c = (3,4)
	elif n == 15:
		c = (3,5)

	elif n == 16:
		c = (4,1)
	elif n == 17:
		c = (4,2)
	elif n == 18:
		c = (4,3)
	elif n == 19:
		c = (4,4)
	elif n == 20:
		c = (4,5)

	elif n == 21:
		c = (5,1)
	elif n == 22:
		c = (5,2)
	elif n == 23:
		c = (5,3)
	elif n == 24:
		c = (5,4)
	elif n == 25:
		c = (5,5)
	
	return c


def matriz_casilla(coor):
	'''
	Pasa de la representación matricial coor = [x,y] con x la fila (1-5) e y la columna (1-5) a la representacion de posiciones en la grilla dada por el numero n = 1-25 
	'''
	if coor[0] == 1 and coor[1]==1:
		c = 1
	elif coor[0] == 1 and coor[1]==2:
		c = 2
	elif coor[0] == 1 and coor[1]==3:
		c = 3
	elif coor[0] == 1 and coor[1]==4:
		c = 4
	elif coor[0] == 1 and coor[1]==5:
		c = 5

	elif coor[0] == 2 and coor[1]==1:
		c = 6
	elif coor[0] == 2 and coor[1]==2:
		c = 7
	elif coor[0] == 2 and coor[1]==3:
		c = 8
	elif coor[0] == 2 and coor[1]==4:
		c = 9
	elif coor[0] == 2 and coor[1]==5:
		c = 10	

	elif coor[0] == 3 and coor[1]==1:
		c = 11
	elif coor[0] == 3 and coor[1]==2:
		c = 12
	elif coor[0] == 3 and coor[1]==3:
		c = 13
	elif coor[0] == 3 and coor[1]==4:
		c = 14
	elif coor[0] == 3 and coor[1]==5:
		c = 15		

	elif coor[0] == 4 and coor[1]==1:
		c = 16
	elif coor[0] == 4 and coor[1]==2:
		c = 17
	elif coor[0] == 4 and coor[1]==3:
		c = 18
	elif coor[0] == 4 and coor[1]==4:
		c = 19
	elif coor[0] == 4 and coor[1]==5:
		c = 20

	elif coor[0] == 5 and coor[1]==1:
		c = 21
	elif coor[0] == 5 and coor[1]==2:
		c = 22
	elif coor[0] == 5 and coor[1]==3:
		c = 23
	elif coor[0] == 5 and coor[1]==4:
		c = 24
	elif coor[0] == 5 and coor[1]==5:
		c = 25

	
	return c 

def casillas_vecinas(c):
	'''
	Devuelve las listas:
	vecinas_p_2: casillas inmediatamente vecinas a la casilla c
	vecinas_p_1: casillas que se encuentran a una casilla de distancia de la casilla c
	'''
	if c == 1:
		vecinas_p_2 = [2,6,7]
		vecinas_p_1 = [3,8,13,12,11]
	if c == 2:
		vecinas_p_2 = [1,3,6,7,8]
		vecinas_p_1 = [11,12,23,14,9,4]
	if c == 3:
		vecinas_p_2 = [2,7,8,9,4]
		vecinas_p_1 = [1,6,11,12,13,14,15,10,5]
	if c == 4:
		vecinas_p_2 = [3,5,8,9,10]
		vecinas_p_1 = [2,7,12,13,14,15]
	if c == 5:
		vecinas_p_2 = [4,9,10]
		vecinas_p_1 = [3,8,13,14,15]
	if c == 6:
		vecinas_p_2 = [1,2,7,12,11]
		vecinas_p_1 = [3,8,13,18,16,17]
	if c == 7:
		vecinas_p_2 = [1,2,3,6,8,11,12,13]
		vecinas_p_1 = [4,9,14,19,18,17,16]
	if c == 8:
		vecinas_p_2 = [2,3,4,7,9,12,13,14]
		vecinas_p_1 = [1,6,11,16,17,18,19,20,15,10,5]
	if c == 9:
		vecinas_p_2 = [3,4,5,8,10,13,14,15]
		vecinas_p_1 = [2,7,12,17,18,19,20]
	if c == 10:
		vecinas_p_2 = [4,5,9,14,15]
		vecinas_p_1 = [3,8,13,18,19,20]
	if c == 11:
		vecinas_p_2 = [6,7,12,17,16]
		vecinas_p_1 = [1,2,3,8,13,18,23,22,21]
	if c == 12:
		vecinas_p_2 = [6,7,8,11,13,16,17,18]
		vecinas_p_1 = [1,2,3,4,9,14,19,24,23,22,21]
	if c == 14:
		vecinas_p_2 = [8,9,10,13,15,18,19,20]
		vecinas_p_1 = [2,3,4,5,7,12,17,22,23,24,25]
	if c == 15:
		vecinas_p_2 = [9,10,14,19,20]
		vecinas_p_1 = [5,4,3,8,13,18,23,24,25]
	if c == 16:
		vecinas_p_2 = [11,12,17,21,22]
		vecinas_p_1 = [6,7,8,13,18,23]
	if c == 17:
		vecinas_p_2 = [11,12,13,16,18,21,22,23]
		vecinas_p_1 = [6,7,8,9,14,19,24]
	if c == 18:
		vecinas_p_2 = [12,13,14,15,17,19,22,23,24]
		vecinas_p_1 = [6,7,8,9,10,11,15,16,20,21,25]
	if c == 19:
		vecinas_p_2 = [13,14,15,18,20,23,24,25]
		vecinas_p_1 = [10,9,8,7,12,17,22]
	if c == 20:
		vecinas_p_2 = [14,15,19,24,25]
		vecinas_p_1 = [10,9,8,13,18,23]
	if c == 21:
		vecinas_p_2 = [16,17,22]
		vecinas_p_1 = [11,12,13,18,23]
	if c == 22:
		vecinas_p_2 = [21,16,17,18,23]
		vecinas_p_1 = [11,12,13,14,19,24]
	if c == 23:
		vecinas_p_2 = [22,17,18,19,24]
		vecinas_p_1 = [21,16,11,12,13,14,15,20,25]
	if c == 24:
		vecinas_p_2 = [23,18,19,20,25]
		vecinas_p_1 = [22,17,12,13,14,15]
	if c == 25:
		vecinas_p_2 = [24,19,20]
		vecinas_p_1 = [23,18,13,14,15]
	return vecinas_p_2, vecinas_p_1

