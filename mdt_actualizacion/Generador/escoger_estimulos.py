#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""escoger_estimulos.py:
   Escoge estímulos aleatoriamente.
"""
# =============================================================================
# Imports
# =============================================================================
from random import choice
# =============================================================================
# Métodos: generales
# =============================================================================

def escoger_estimulos(demanda,estimulos):
	'''
	Funcion que recibe el numero de demanda y la lista de los nombres de los estimulos y devuelve una lista de estimulos escogidos aleatoriamente, siguiendo las condiciones determinadas.
	'''
	if demanda == 1: 
		estimulo_1 = choice(estimulos)
		escogidos = [estimulo_1]
	elif demanda == 2:
		estimulo_1 = choice(estimulos)
		while True:
			estimulo_2 = choice(estimulos)
			#Utiliza la funcion vocales para no escoger estimulos cuyos nombres tengan la misma combinacion de vocales
			if vocales(estimulo_1) != vocales(estimulo_2):
				escogidos = [estimulo_1,estimulo_2]
				break
	elif demanda ==3:
		estimulo_1 = choice(estimulos)
		vocales_1 = vocales(estimulo_1)
		while True:
			estimulo_2 = choice(estimulos)
			estimulo_3 = choice(estimulos)
			vocales_2 = vocales(estimulo_2)
			vocales_3 = vocales(estimulo_3)
			#Utiliza la funcion vocales para no escoger estimulos cuyos nombres tengan la misma combinacion de vocales
			if vocales_1 != vocales_2 and vocales_1 != vocales_3 and vocales_2 != vocales_3:
				escogidos = [estimulo_1,estimulo_2,estimulo_3]
				break
	else:
		estimulo_1 = choice(estimulos)
		vocales_1 = vocales(estimulo_1)
		while True:
			estimulo_2 = choice(estimulos)
			estimulo_3 = choice(estimulos)
			estimulo_4 = choice (estimulos)
			vocales_2 = vocales(estimulo_2)
			vocales_3 = vocales(estimulo_3)
			vocales_4 = vocales(estimulo_4)
			if vocales_1 != vocales_2 and vocales_1 != vocales_3 and vocales_1 != vocales_4  and vocales_2 != vocales_3 and vocales_2 != vocales_4 and vocales_3 != vocales_4:
				escogidos = [estimulo_1,estimulo_2,estimulo_3, estimulo_4]
				break
	
	return escogidos

def vocales(string):
	'''
	Funcion de identificacion de vocales utilizada en los criterios de escogencia de la funcion escoger_estimulos.
	#Devuelve la secuencia de vocales de la palabra. Identifica la sección 'que' de una palabra como la vocal 'e'
	'''
	string = string.replace("que","e")
	vocales = []
	for letra in string:
		if letra == 'a' or letra == 'e' or letra == 'i' or letra == 'o' or letra == 'u':
			vocales.append(letra)	 
	return vocales
