#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""escritor.py:
   Este archivo contiene las funciones que permiten escribir en el archivo que
   se genera para guardar los datos del participante.
"""
# =============================================================================
# Imports
# =============================================================================
import os
from time import strftime
import constantes as ctes
# =============================================================================
# Métodos: generales
# =============================================================================

def iniciar(SUJETO, PROYECTO, FORMA):
	'''
	Inicializa el archivo a utilizar para guardar los resultados del participante. Además guarda el encabezado respectivo.
	'''
	global archivo
	if not os.path.exists(ctes.EVALUACIONES_PATH):
		os.makedirs(ctes.EVALUACIONES_PATH)
	C_P = SUJETO+'--proyecto-'+PROYECTO+ '--forma-'+FORMA+'--' + strftime("%d-%m-%Y") +'--'+ strftime("%H-%M-%S")
	archivo = open(ctes.RESULT_PARTICIPANTE_PATH + C_P + '.csv','w')
	archivo.write('ID,NAME,F_D_N,AGE,F_D_E,SEX,GRADE,EVAL,FORM,FE')
	archivo.write(',ENSAYO,SET,DEMAND,CL')
	archivo.write(',PROCESS,STIMULUS,POS.STIM,POS.RESPONSE,COORD.RESPONSE,TIME,ACC,VALIDITY,TOTAL.TIME')
	archivo.write('\n')
	return archivo, C_P

def cerrar(tiempototal):
	'''
	Guarda tiempo total al final del archivo y lo cierra.
	'''
	global archivo
	# archivo.seek(-2, os.SEEK_END)
	archivo.seek(0, os.SEEK_END)
	archivo.seek(archivo.tell() -2, os.SEEK_SET)
	archivo.truncate()
	archivo.write(","+str(tiempototal) + ' min')
	archivo.close()

def escribir(demanda,w,d):
	'''
	Le entran las siguientes variables:
	demanda: la demanda del ensayo 1-3
	archivo: listas con los datos generales
	d: el numero de ensayo
	
	Note:
	w: hace referencia a los elementos devueltos dentro del ensayo:	
		#0: ESTIMULOS 
		#1: POS_ESTIMULOS 
		#2: T_CODIFICACION 
		#3: FLECHAS 
		#4: RESULTADOS_ACT 
		#5: T_ACT 
		#6: CASILLAS_ACT 
		#7: POSICION_CLICK_ACT 
		#8: VALIDEZ 
		#9: RESULTADOS_RECALL 
		#10: T_RECALL 
		#11: CASILLAS_RECALL 	
		#12: POSICION_BOLA
		#13: POSICION_CAMBIO
	'''
	global archivo

	n_EC1 = str(w[0][0])
	PEC_1 = str(w[1][0])
	RTI_1 = str(w[2][0])
	F1_1 = str(w[3][0][0])
	F1_2 = str(w[3][0][1])
	F1_3 = str(w[3][0][2])
	EC1_1 = str(w[4][0][0])
	EC1_2 = str(w[4][0][1])
	EC1_3 = str(w[4][0][2])
	PB1_1 = str(w[12][0][0])
	CB1_1 = str(w[13][0][0])
	PB1_2 = str(w[12][0][1])
	CB1_2 = str(w[13][0][1])
	PB1_3 = str(w[12][0][2])
	CB1_3 = str(w[13][0][2])
	RT1_E1 = str(w[5][0][0])
	RT1_E2 =str(w[5][0][1])
	RT1_E3 = str(w[5][0][2])
	PRC1_1 = str(w[6][0][0])
	PRC1_2 =str(w[6][0][1])
	PRC1_3 = str(w[6][0][2])
	CRC1_1X = str(w[7][0][0][0])
	CRC1_2X = str(w[7][0][1][0])
	CRC1_3X = str(w[7][0][2][0])
	CRC1_1Y = str(w[7][0][0][1])
	CRC1_2Y = str(w[7][0][1][1])
	CRC1_3Y = str(w[7][0][2][1])
	VD_1 = str(w[8][0])
	RECALL_1 = str(w[9][0])
	TR_RECALL_1 = str(w[10][0])
	PCR_1 = str(w[11][0])	

	n_EC2 = str(w[0][1])
	PEC_2 = str(w[1][1])
	RTI_2 = str(w[2][1])
	F2_1 = str(w[3][1][0])
	F2_2 = str(w[3][1][1])
	F2_3 = str(w[3][1][2])
	EC2_1 = str(w[4][1][0])
	EC2_2 = str(w[4][1][1])
	EC2_3 = str(w[4][1][2])
	PB2_1 = str(w[12][1][0])
	CB2_1 = str(w[13][1][0])
	PB2_2 = str(w[12][1][1])
	CB2_2 = str(w[13][1][1])
	PB2_3 = str(w[12][1][2])
	CB2_3 = str(w[13][1][2])
	RT2_E1 = str(w[5][1][0])
	RT2_E2 = str(w[5][1][1])
	RT2_E3 = str(w[5][1][2])
	PRC2_1 = str(w[6][1][0])
	PRC2_2 = str(w[6][1][1])
	PRC2_3 = str(w[6][1][2])
	CRC2_1X = str(w[7][1][0][0])
	CRC2_2X =str(w[7][1][1][0])
	CRC2_3X = str(w[7][1][2][0])
	CRC2_1Y = str(w[7][1][0][1])
	CRC2_2Y = str(w[7][1][1][1])
	CRC2_3Y = str(w[7][1][2][1])
	VD_2 = str(w[8][1])
	RECALL_2 = str(w[9][1])
	TR_RECALL_2 = str(w[10][1])
	PCR_2 = str(w[11][1])

	n_EC3 = str(w[0][2])		
	PEC_3 = str(w[1][2])	
	RTI_3 = str(w[2][2])	
	F3_1 = str(w[3][2][0])
	F3_2 = str(w[3][2][1])
	F3_3 = str(w[3][2][2])	
	EC3_1 = str(w[4][2][0])
	EC3_2 = str(w[4][2][1])
	EC3_3 = str(w[4][2][2])
	PB3_1 = str(w[12][2][0])
	CB3_1 = str(w[13][2][0])
	PB3_2 = str(w[12][2][1])
	CB3_2 = str(w[13][2][1])
	PB3_3 = str(w[12][2][2])
	CB3_3 = str(w[13][2][2])
	RT3_E1 = str(w[5][2][0])
	RT3_E2 = str(w[5][2][1])
	RT3_E3 = str(w[5][2][2])
	PRC3_1 = str(w[6][2][0])
	PRC3_2 = str(w[6][2][1])
	PRC3_3 = str(w[6][2][2])
	CRC3_1X = str(w[7][2][0][0])
	CRC3_2X = str(w[7][2][1][0])
	CRC3_3X = str(w[7][2][2][0])
	CRC3_1Y = str(w[7][2][0][1])
	CRC3_2Y = str(w[7][2][1][1])
	CRC3_3Y = str(w[7][2][2][1])
	VD_3 = str(w[8][2])
	RECALL_3 = str(w[9][2])
	TR_RECALL_3 = str(w[10][2])
	PCR_3 = str(w[11][2])

	n_EC4 = str(w[0][3])		
	PEC_4 = str(w[1][3])	
	RTI_4 = str(w[2][3])	
	F4_1 = str(w[3][3][0])
	F4_2 = str(w[3][3][1])
	F4_3 = str(w[3][3][2])	
	EC4_1 = str(w[4][3][0])
	EC4_2 = str(w[4][3][1])
	EC4_3 = str(w[4][3][2])
	PB4_1 = str(w[12][3][0])
	CB4_1 = str(w[13][3][0])
	PB4_2 = str(w[12][3][1])
	CB4_2 = str(w[13][3][1])
	PB4_3 = str(w[12][3][2])
	CB4_3 = str(w[13][3][2])
	RT4_E1 = str(w[5][3][0])
	RT4_E2 = str(w[5][3][1])
	RT4_E3 = str(w[5][3][2])
	PRC4_1 = str(w[6][3][0])
	PRC4_2 = str(w[6][3][1])
	PRC4_3 = str(w[6][3][2])
	CRC4_1X = str(w[7][3][0][0])
	CRC4_2X = str(w[7][3][1][0])
	CRC4_3X = str(w[7][3][2][0])
	CRC4_1Y = str(w[7][3][0][1])
	CRC4_2Y = str(w[7][3][1][1])
	CRC4_3Y = str(w[7][3][2][1])
	VD_4 = str(w[8][3])
	RECALL_4 = str(w[9][3])
	TR_RECALL_4 = str(w[10][3])
	PCR_4 = str(w[11][3])
		
	archivo.write(d+',Codificacion 1,'+n_EC1+','+PEC_1+',null,null,'+RTI_1+',null,null'+'\n')
	archivo.write(d+',Mostrar Bola 1_1,Bola Verde'+','+PB1_1+','+'null,null,null,null,null'+'\n')
	archivo.write(d+',Respuesta Bola 1_1,'+F1_1+','+CB1_1+','+PRC1_1+',('+CRC1_1X+'_'+CRC1_1Y+'),'+RT1_E1+','+EC1_1+',null'+'\n')
	archivo.write(d+',Mostrar Bola 2_1,Bola Verde'+','+PB2_1+','+'null,null,null,null,null'+'\n')
	archivo.write(d+',Respuesta Bola 2_1,'+F2_1+','+CB2_1+','+PRC2_1+',('+CRC2_1X+'_'+CRC2_1Y+'),'+RT2_E1+','+EC2_1+',null'+'\n')
	archivo.write(d+',Mostrar Bola 3_1,Bola Verde'+','+PB3_1+','+'null,null,null,null,null'+'\n')
	archivo.write(d+',Respuesta Bola 3_1,'+F3_1+','+CB3_1+','+PRC3_1+',('+CRC3_1X+'_'+CRC3_1Y+'),'+RT3_E1+','+EC3_1+',null'+'\n')
	if demanda == 2 or demanda == 3:
		archivo.write(d+',Codificacion 2,'+n_EC2+','+PEC_2+',null,null,'+RTI_2+',null,null'+'\n')
		archivo.write(d+',Mostrar Bola 1_2,Bola Verde'+','+PB1_2+','+'null,null,null,null,null'+'\n')
		archivo.write(d+',Respuesta Bola 1_2,'+F1_2+','+CB1_2+','+PRC1_2+',('+CRC1_2X+'_'+CRC1_2Y+'),'+RT1_E2+','+EC1_2+',null'+'\n')
		archivo.write(d+',Mostrar Bola 2_2,Bola Verde'+','+PB2_2+','+'null,null,null,null,null'+'\n')
		archivo.write(d+',Respuesta Bola 2_2,'+F2_2+','+CB2_2+','+PRC2_2+',('+CRC2_2X+'_'+CRC2_2Y+'),'+RT2_E2+','+EC2_2+',null'+'\n')
		archivo.write(d+',Mostrar Bola 3_2,Bola Verde'+','+PB3_2+','+'null,null,null,null,null'+'\n')
		archivo.write(d+',Respuesta Bola 3_2,'+F3_2+','+CB3_2+','+PRC3_2+',('+CRC3_2X+'_'+CRC3_2Y+'),'+RT3_E2+','+EC3_2+',null'+'\n')
	if demanda == 3:
		archivo.write(d+',Codificacion 3,'+n_EC3+','+PEC_3+',null,null,'+RTI_3+',null,null'+'\n')
		archivo.write(d+',Mostrar Bola 1_3,Bola Verde'+','+PB1_3+','+'null,null,null,null,null'+'\n')
		archivo.write(d+',Respuesta Bola 1_3,'+F1_3+','+CB1_3+','+PRC1_3+',('+CRC1_3X+'_'+CRC1_3Y+'),'+RT1_E3+','+EC1_3+',null'+'\n')
		archivo.write(d+',Mostrar Bola 2_3,Bola Verde'+','+PB2_3+','+'null,null,null,null,null'+'\n')
		archivo.write(d+',Respuesta Bola 2_3,'+F2_3+','+CB2_3+','+PRC2_3+',('+CRC2_3X+'_'+CRC2_3Y+'),'+RT2_E3+','+EC2_3+',null'+'\n')
		archivo.write(d+',Mostrar Bola 3_3,Bola Verde'+','+PB3_3+','+'null,null,null,null,null'+'\n')
		archivo.write(d+',Respuesta Bola 3_3,'+F3_3+','+CB3_3+','+PRC3_3+',('+CRC3_3X+'_'+CRC3_3Y+'),'+RT3_E3+','+EC3_3+',null'+'\n')
	archivo.write(d+',RECUPERACION 1,'+n_EC1+','+PEC_1+','+PCR_1+',null,'+TR_RECALL_1+','+RECALL_1+','+VD_1+'\n')
	if demanda == 2 or demanda == 3:
		archivo.write(d+',RECUPERACION 2,'+n_EC2+','+PEC_2+','+PCR_2+',null,'+TR_RECALL_2+','+RECALL_2+','+VD_2+'\n')
	if demanda == 3:	
		archivo.write(d+',RECUPERACION 3,'+n_EC3+','+PEC_3+','+PCR_3+',null,'+TR_RECALL_3+','+RECALL_3+','+VD_3+'\n')
