#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""auxiliares.py:
   Este archivo contiene funciones auxiliares que no pertenecen a otro módulo
   de mayor importancia.
"""
# =============================================================================
# Imports
# =============================================================================
import time
import calendar
import pygame
import logging
from math import floor
from datetime import date, datetime
from time import strftime
from tkinter import *
import tkinter.ttk
import datos
import constantes as ctes
# =============================================================================
# Métodos: Tiempo existido
# =============================================================================

def tiempo_existido(y,m,d):
	'''
	Calcula la edad de una persona dada su fecha de nacimiento.
	Calcula la edad en años, meses y días.
	'''
	#Obtiene la fecha del día actual
	y = int(y)
	m = int(m)
	d = int(d)
	ynow = date.today().year
	mnow = date.today().month
	dnow = date.today().day
	
	#Calcula diferencia de meses entre el año 0, mes 0, dia 0 hasta la fecha de nacimiento y la fe3cha actual y calcula su diferencia
	t0 = 12*y + m - 1
	t = ynow*12 + mnow - 1
	dm = t - t0
	
	if(dnow >= d):
		#Si el dia del mes actual es mayor o igual que el día del mes de nacimiento, ya esta la edad
		f = [int(floor(dm/12)),dm%12,dnow-d]

	else:
		#Si el dia del mes es menor al dia del mes de nacimiento:
		dm = dm - 1
		t = t - 1
		
		t_mod = t%12 + 1
		t_floor = int(floor(t/12))
		
		z = str(d)+'/'+str(t_mod)+'/'+str(t_floor)
		try:
			fecha = datetime.strptime(z,"%d/%m/%Y")
		except:
			if t_mod in [4,6,9,11]:
				z = str(30)+'/'+str(t_mod)+'/'+str(t_floor)
			elif t_mod == 2:
				if calendar.isleap(t_floor):
					z = str(29)+'/'+str(t_mod)+'/'+str(t_floor)
				else:
					z = str(28)+'/'+str(t_mod)+'/'+str(t_floor)
					
			fecha = datetime.strptime(z,"%d/%m/%Y")
		
		dfeh = time.mktime(date.today().timetuple())
		dfe = time.mktime(fecha.timetuple())
		
		f = [int(floor(dm/12)),dm%12,int((dfeh-dfe)/60/60/24)]
	
	return f

# =============================================================================
# Métodos: Grilla
# =============================================================================
		
def grilla(screen,cent):
	'''
	Se encarga de dibujar la grilla
	Use:
		screen: display 
		cent: coordenadas del centro de la pantalla
	
	'''
	mx = cent[0] - 55
	my = cent[1] - 55
	
	r1 = pygame.draw.rect(screen, (0,0,0), (mx-220,my-220, 110,110),1)
	r2 = pygame.draw.rect(screen, (0,0,0), (mx-110, my-220, 110,110),1)
	r3 = pygame.draw.rect(screen, (0,0,0), (mx,my-220, 110,110),1)
	r4 = pygame.draw.rect(screen, (0,0,0), (mx+110,my-220, 110,110),1)
	r5 = pygame.draw.rect(screen, (0,0,0), (mx+220,my-220,110,110),1)
	
	r6 = pygame.draw.rect(screen, (0,0,0), (mx-220,my-110, 110,110),1)
	r7 = pygame.draw.rect(screen, (0,0,0), (mx-110, my-110, 110,110),1)
	r8 = pygame.draw.rect(screen, (0,0,0), (mx,my-110, 110,110),1)
	r9 = pygame.draw.rect(screen, (0,0,0), (mx+110,my-110, 110,110),1)
	r10 = pygame.draw.rect(screen, (0,0,0), (mx+220,my-110,110,110),1)
	
	r11= pygame.draw.rect(screen, (0,0,0), (mx-220,my, 110,110),1)
	r12 = pygame.draw.rect(screen, (0,0,0), (mx-110, my, 110,110),1)
	r13 = pygame.draw.rect(screen, (0,0,0), (mx,my, 110,110),1)
	r14 = pygame.draw.rect(screen, (0,0,0), (mx+110,my, 110,110),1)
	r15 = pygame.draw.rect(screen, (0,0,0), (mx+220,my,110,110),1)
	
	r16 = pygame.draw.rect(screen, (0,0,0), (mx-220,my+110, 110,110),1)
	r17 = pygame.draw.rect(screen, (0,0,0), (mx-110, my+110, 110,110),1)
	r18 = pygame.draw.rect(screen, (0,0,0), (mx,my+110, 110,110),1)
	r19 = pygame.draw.rect(screen, (0,0,0), (mx+110,my+110, 110,110),1)
	r20 = pygame.draw.rect(screen, (0,0,0), (mx+220,my+110,110,110),1)
	
	r21 = pygame.draw.rect(screen, (0,0,0), (mx-220,my+220, 110,110),1)
	r22 = pygame.draw.rect(screen, (0,0,0), (mx-110, my+220, 110,110),1)
	r23 = pygame.draw.rect(screen, (0,0,0), (mx,my+220, 110,110),1)
	r24 = pygame.draw.rect(screen, (0,0,0), (mx+110,my+220, 110,110),1)
	r25 = pygame.draw.rect(screen, (0,0,0), (mx+220,my+220,110,110),1)
	
# =============================================================================
# Métodos: Caja de comentarios
# =============================================================================

def on_closing():
	'''
	Cerrar ventana.
	'''
	global i
	i = 1
	root.destroy()
		
def save():
	'''
	Guardar comentarios en archivo respectivo.
	'''
	txt = Comentarios.get(1.0,tkinter.END)
	if txt != '\n':
		arch = open(flnm,'w+')
		arch.write(txt)
		arch.close()
	root.destroy()

def caja_comentarios(filename):
	'''
	Presentar caja de comentarios.
	'''
	if ctes.CORRER_CAJA_COMENTARIOS:
		logging.info('Corriendo caja de comentarios...')
		global root, flnm, Comentarios
		flnm = filename
		root =Tk()
		root.wm_title("Comentarios")
		mainframe=tkinter.ttk.Frame(root, padding= "3 3 9 9")
		mainframe.grid(column=0, row=0)
		mainframe.columnconfigure(0, weight=1)
		mainframe.rowconfigure(0,weight=1)
		size = (root.winfo_screenwidth(), root.winfo_screenheight())
		
		global COMENTARIOS, continuar
		
		COMENTARIOS = StringVar()	
		comentarios=tkinter.ttk.Label(mainframe,text='Comentarios')
		comentarios.grid(sticky=E)
		Comentarios = tkinter.Text(root,height=20, width=40)
		Comentarios.grid(sticky=S)
		continuar = tkinter.ttk.Button(mainframe, text='Ok', command= save)
		continuar.grid(sticky=S)

		global i
		i = 0
		root.resizable(0,0)
		root.protocol("WM_DELETE_WINDOW", on_closing)
		root.mainloop()	
		
		com = COMENTARIOS.get()
		logging.info('Fin de la caja de comentarios...')
		
		return com
	else:
		logging.info('Caja de comentarios desactivada...')