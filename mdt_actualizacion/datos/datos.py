#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""datos.py:
   Este archivo contiene las funciones que permiten extraer los datos del par-
   ticipante, mostrando un cuadro en pantalla. Esta se corre únicamente si la 
   variable CORRER_DATOS_SUJETOS es True, ubicada en el archivo constantes.py.
"""
# =============================================================================
# Imports
# =============================================================================
import sys
import logging
from tkinter import *
import tkinter.ttk
from time import strftime
import auxiliares as aux
import constantes as ctes
# =============================================================================
# Métodos: generales
# =============================================================================

def extraer_datos_participante():
	'''
	Se corre la funcion que pide los datos del sujeto que realiza la prueba. Se ejecuta un control que verifica que las entradas de SUJETO y EVALUADOR sean numericos. Si algun valor dado en el formulario no es valido, se repite la funcion hasta que todos los datos sean validos. En caso de que en ajustes se desactive el formulario de datos, se asignan valores genericos.
	'''
	if not ctes.CORRER_DATOS_SUJETOS:
		logging.info('Datos del participante definidos como PRUEBA...')
		NOMBRE, F_D_N, SEXO, GRADO, SUJETO, FORMA, DESTROY, EVALUADOR, EDAD, PROYECTO = 'PREUBA', 'PRUEBA', 'PRUEBA', 'PRUEBA', '999', '2' , 0, 'PRUEBA', 'PRUEBA', 'PRUEBA'
		F_D_E = strftime("%Y-%m-%d")
		
	else:
		logging.info('Extrayendo datos del participante...')
		done = False
		while not done:
			NOMBRE, F_D_N, SEXO, GRADO, SUJETO, FORMA, DESTROY, EVALUADOR, PROYECTO = datos()
			F_D_E = strftime("%Y-%m-%d")
			EDAD = aux.tiempo_existido(F_D_N[0:4],F_D_N[5:7],F_D_N[8:11])
			EDAD = str(EDAD[0]) + ' a - ' + str(EDAD[1]) + ' m - ' + str(EDAD[2]) + ' d'

			if DESTROY == 1:
					sys.exit()

			if SUJETO.isdigit() and EVALUADOR.isdigit():
				try:
					test = open(ctes.ENSAYOS1_PATH,'r')
					test.close()
					done = True
				except:
					error_prueba()		
			else:
				error_digitos()
		logging.info('Datos del participante listos...')
	
	return NOMBRE, F_D_N, SEXO, GRADO, SUJETO, FORMA, DESTROY, EVALUADOR, EDAD, PROYECTO, F_D_E


def buttonstate(*event):
	'''
	Definir estado del botón.
	'''
	proy = PROYECTO.get()
	nom = NOMBRE.get()
	d = DIA.get()
	m = MES.get()
	an = A.get()
	g = GRADO.get()
	s = SUJETO.get()
	sx = SEXO.get()
	#p = PRUEBA.get()
	f = FORMULA.get()
	ev = EVALUADOR.get()

	if nom != '' and d != 'Día' and m != 'Mes' and an != 'Año' and g != 'Grado' and s != '' and sx != ''  and f != '' and ev!='' and proy != '':
		continuar.config(state=NORMAL)
	else:
		continuar.config(state=DISABLED)

def on_closing():
	'''
	Cerrar cuadro de diálogo.
	'''
	global i
	i = 1
	root.destroy()

def datos():
	'''
	Muestra cuadro de diálogo de datos del participante.
	'''
	global root
	root =Tk()
	root.wm_title("Datos de sujeto")
	mainframe=tkinter.ttk.Frame(root, padding= "3 3 12 12")
	mainframe.grid(column=0, row=0,)
	mainframe.columnconfigure(0, weight=1)
	mainframe.rowconfigure(0,weight=1)

	global NOMBRE, DIA, MES, A, SEXO, GRADO, SUJETO, FORMULA, continuar, EVALUADOR, PROYECTO

	PROYECTO = StringVar()
	NOMBRE = StringVar()
	DIA = StringVar()
	MES= StringVar()
	A= StringVar()#Año
	SEXO = StringVar()
	GRADO = StringVar()
	SUJETO = StringVar()
	#PRUEBA = StringVar()
	FORMULA = StringVar()
	EVALUADOR = StringVar()

	nombre=tkinter.ttk.Label(mainframe,text='Nombre')
	nombre.grid(column=1,row=1)
	Nombre=tkinter.ttk.Entry(mainframe, textvariable=NOMBRE)
	Nombre.grid(column=2,row=1, padx=20, pady=10)

	fecha=tkinter.ttk.Label(mainframe, text='Fecha de Nacimiento')
	fecha.grid(column=1,row=2,padx=5)

	dia = tkinter.ttk.Combobox(mainframe,textvariable=DIA, state= 'readonly', width='10')
	dia['values']=list(range(1,32))
	dia.grid(column=2,row=2)
	dia.set('Día')

	mes = tkinter.ttk.Combobox(mainframe,textvariable=MES, state= 'readonly', width='10')
	mes['values']=list(range(1,13))
	mes.grid(column=3,row=2, pady= 0)
	mes.set('Mes')

	a = tkinter.ttk.Combobox(mainframe,textvariable=A, state= 'readonly', width='10')
	a['values']=list(range(2000,int(strftime("%Y"))+1))
	a.grid(column=4,row=2,padx=15)
	a.set('Año')

	sexo=tkinter.ttk.Label(mainframe, text='Sexo')
	sexo.grid(column=1,row=3,padx=5, pady = 5)
	Masculino = tkinter.ttk.Radiobutton(mainframe, text='Masculino', variable=SEXO, value='masculino')
	Masculino.grid(column=2, row=3,pady=5)
	Feminino = tkinter.ttk.Radiobutton(mainframe, text='Femenino', variable=SEXO, value='femenino')
	Feminino.grid(column=3, row=3,pady=5)
	Otro = tkinter.ttk.Radiobutton(mainframe, text='Otro', variable=SEXO, value='otro')
	Otro.grid(column=4, row=3,pady=5)

	grado=tkinter.ttk.Label(mainframe, text='Grado')
	grado.grid(column=1,row=4,padx=5, pady = 5)
	Grado = tkinter.ttk.Combobox(mainframe,textvariable=GRADO, state= 'readonly', width='10')
	Grado['values']=list(range(1,7))
	Grado.grid(column=2,row=4)
	Grado.set('Grado')

	sujeto = tkinter.ttk.Label(mainframe, text='Sujeto')
	sujeto.grid(column = 1, row= 5)
	Sujeto = tkinter.ttk.Entry(mainframe, textvariable=SUJETO)
	Sujeto.grid(column=2,row=5, padx=20, pady=10)

	#prueba = ttk.Label(mainframe, text='Prueba')
	#prueba.grid(column = 1, row= 6)
	#Prueba = ttk.Entry(mainframe, textvariable=PRUEBA)
	#Prueba.grid(column=2,row=6, padx=20, pady=10)


	formula = tkinter.ttk.Label(mainframe, text='Formula')
	formula.grid(column = 1, row= 7)
	uno = tkinter.ttk.Radiobutton(mainframe, text='1', variable=FORMULA, value='1')
	uno.grid(column=2, row=7,pady=5)
	dos = tkinter.ttk.Radiobutton(mainframe, text='2', variable=FORMULA, value='2')
	dos.grid(column=3, row=7,pady=5)
	tres = tkinter.ttk.Radiobutton(mainframe, text='3', variable=FORMULA, value='3')
	tres.grid(column=4, row=7,pady=5)

	evaluador=tkinter.ttk.Label(mainframe, text='Evaluador')
	evaluador.grid(column=1,row=8,padx=5, pady = 5)
	Evaluador = tkinter.ttk.Entry(mainframe,textvariable=EVALUADOR, width='10')
	Evaluador.grid(column=2,row=8)

	#proyecto=ttk.Label(mainframe, text='Numero de proyecto')
	#proyecto.grid(column=1,row=9,padx=5, pady = 5)
	#Proyecto = ttk.Entry(mainframe,textvariable=PROYECTO, width='10')
	#Proyecto.grid(column=2,row=9)

	proyecto=tkinter.ttk.Label(mainframe, text='Proyecto')
	proyecto.grid(column=1,row=9,padx=5, pady = 5)
	Proyecto = tkinter.ttk.Combobox(mainframe,textvariable=PROYECTO, state= 'readonly', width='15')
	Proyecto['values']=['837-B5-301','837-B7-336','TFG-MR']
	Proyecto.grid(column=2,row=9)
	Proyecto.set('Proyecto')

	continuar = tkinter.ttk.Button(mainframe, text='Comenzar', state= DISABLED, command= root.destroy)
	continuar.grid(column=4,row=10, pady= 5)

	PROYECTO.trace('w',buttonstate)
	NOMBRE.trace('w',buttonstate)
	DIA.trace('w',buttonstate)
	MES.trace('w',buttonstate)
	A.trace('w',buttonstate)
	SEXO.trace('w',buttonstate)
	GRADO.trace('w',buttonstate)
	SUJETO.trace('w',buttonstate)
	#PRUEBA.trace('w',buttonstate)
	FORMULA.trace('w',buttonstate)
	EVALUADOR.trace('w',buttonstate)

	global i
	i = 0
	root.resizable(0,0)
	root.protocol("WM_DELETE_WINDOW", on_closing)
	root.mainloop()

	proy = PROYECTO.get()
	nom = NOMBRE.get()
	d = DIA.get()
	if len(d) == 1:
		d = '0'+d
	m = MES.get()
	if len(m) == 1:
		m = '0'+m
	an = A.get()
	fdn = an+'-'+m+'-'+d
	g = GRADO.get()
	s = SUJETO.get()
	sx = SEXO.get()
	#p = PRUEBA.get()
	f = FORMULA.get()
	ev = EVALUADOR.get()

	return nom, fdn, sx, g, s, f, i,ev, proy

def error_digitos():
	global root
	root =Tk()
	root.wm_title("Error")
	mainframe=tkinter.ttk.Frame(root, padding= "3 3 12 12")
	mainframe.grid(column=0, row=0,)
	mainframe.columnconfigure(0, weight=1)
	mainframe.rowconfigure(0,weight=1)

	mensaje=tkinter.ttk.Label(mainframe, text='Los valores en los campos "Sujeto" y "Evaluador" deben ser numericos.')
	mensaje.grid(column=1,row=3,padx=5)

	continuar = tkinter.ttk.Button(mainframe, text='Ok', command= root.destroy)
	continuar.grid(column=2,row=5, pady= 5)

	root.resizable(0,0)
	root.protocol("WM_DELETE_WINDOW", on_closing)
	root.mainloop()

def error_prueba():
	global root
	root =Tk()
	root.wm_title("Error")
	mainframe=tkinter.ttk.Frame(root, padding= "3 3 12 12")
	mainframe.grid(column=0, row=0,)
	mainframe.columnconfigure(0, weight=1)
	mainframe.rowconfigure(0,weight=1)

	mensaje=tkinter.ttk.Label(mainframe, text='No existe la prueba indicada.')
	mensaje.grid(column=1,row=3,padx=5)

	continuar = tkinter.ttk.Button(mainframe, text='Ok', command= root.destroy)
	continuar.grid(column=2,row=5, pady= 5)

	root.resizable(0,0)
	root.protocol("WM_DELETE_WINDOW", on_closing)
	root.mainloop()
