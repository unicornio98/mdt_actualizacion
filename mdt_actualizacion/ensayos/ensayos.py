#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# Created By  : 
# Created Date: Mon August 7 15:54:00 CST 2020
# =============================================================================
"""ensayos.py:
   Este archivo contiene las funciones que permiten presentar cada uno de los ensayos del experimento o práctica.
"""
# =============================================================================
# Imports
# =============================================================================
import sys
import pygame
from pygame.locals import *
import escritor
import auxiliares as aux
import constantes as ctes
import coordenadas as coord
import preparacion_global as gbl
# =============================================================================
# Constantes
# =============================================================================

SOUND_NEGATIVE = pygame.mixer.Sound(ctes.NEG_SND_PATH)
SOUND_POSITIVE = pygame.mixer.Sound(ctes.POS_SND_PATH)
PUNTO_FIJACION = pygame.image.load(ctes.PUNTO_FIJ_PATH)
BOLA_VERDE = pygame.image.load(ctes.BOLA_VERDE_PATH)

# =============================================================================
# Métodos: generales
# =============================================================================

def cerrar():
	'''
	Funcion que muestra una una pantalla que pregunta al usuario si quiere cerrar el programa.
	Por la manera en que esta ejecutado, solo se puede acceder presionando la 
	tecla E mientras se este mostrando una flecha en la pantalla.
	
	Use: 
		screen(Layer), 
		(coordenadas del centro de la pantalla)
		archivo
	
	'''
	mx, my = gbl.cent[0],gbl.cent[1]
	done = False
	pygame.mouse.set_visible(0)
	gbl.screen.fill(ctes.WHITE)
	text1 = gbl.font25.render(ctes.SALIR_PRUEBA_MSG,True,(0,0,0))		
	text1_width = text1.get_width()		
	gbl.screen.blit(text1,[mx-text1_width/2,my])
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_s:
					tiempototal = round(gbl.clock_absoluto.tick()/60000.0,2)
					escritor.cerrar(tiempototal)  
					sys.exit()
				elif event.key == pygame.K_n:
					done = True
		pygame.display.flip()
		
def play_sound(t):
	'''
	Reproduce el sonido de error o correcto segun sea el caso.
	Use:
		t: n = incorrecto, p = correcto
		
	'''
	if t == 'n':
		SOUND_NEGATIVE.play()
		pygame.event.pump()
		pygame.time.delay(600)
		SOUND_NEGATIVE.stop()
	elif t == 'p':
		SOUND_POSITIVE.play()
		pygame.event.pump()
		pygame.time.delay(600)
		SOUND_POSITIVE.stop()
	
def punto_de_fijacion(fondo):
	'''
	Muesta el punto de fijacion por 500 ms.
	Use:
		screen
		cent
		fondo
	
	'''
	if fondo == 'blanco':
		f = ctes.WHITE
	elif fondo == 'azul':
		f = ctes.BLUE
	mx, my = gbl.cent[0], gbl.cent[1]
	gbl.screen.fill(f)
	pygame.mouse.set_visible(0)
	gbl.screen.blit(PUNTO_FIJACION, (mx-68,my-68))
	pygame.display.flip()
	pygame.event.pump()
	pygame.time.delay(500)


def actualizacion(POSICION_BOLA, POSICION_CAMBIO, FLECHA, n,carga):
	"""
	Funcion encarga de mostrar la bola en una posicion de la cuadricula, 
	posterior a eso muestra la flecha que indica la direccion de la flecha hacia la cual se moveria 
	la bola, luego se encarga de recoger la respuesta.
	
	Use:
		screen, 
		cent: coordenadas del centro
		POSICION_BOLA: coordenadas en donde se va a mostrar la bola
		POSICION_CAMBIO: posicion donde se moveria la bola, 
		Flecha: la flecha que indica el lugar donde se debe mover la bola, 
		n : el numero de 
		carga
		archivo 
		(contiene datos generales que el escritor debe escribir).

	En lo que respecta a su funcion, se  
	
	return: 
		resultado_act: resultado (1= correcto, 0 = incorrecto)
		tiempo_a: tiempo en responder 
		position: posición

	"""
	POS_A = int(POSICION_BOLA[n-1])
	POS_N = int(POSICION_CAMBIO[n-1])
	c= carga
	mx = gbl.cent[0]
	my = gbl.cent[1]
	
	centro = coord.identificar_coord(POS_N,gbl.cent)
	done = False
	gbl.screen.fill(ctes.WHITE)
	pygame.mouse.set_visible(0)
	gbl.screen.blit(BOLA_VERDE, (coord.identificar_coord(POS_A,gbl.cent)[0]-55,coord.identificar_coord(POS_A,gbl.cent)[1]-55))
	aux.grilla(gbl.screen,gbl.cent)
	pygame.display.flip()
	pygame.event.pump()
	pygame.time.delay(750)
	

	flecha = pygame.image.load( ctes.FLECHAS_PATH + ctes.BANCO_FLECHAS[ (FLECHA[n-1]) ] )
	pygame.event.clear()
	pygame.mouse.set_visible(1)
	pygame.mouse.set_pos((mx,my))
	gbl.clock.tick()
	gbl.screen.fill(ctes.WHITE)
	gbl.screen.blit(flecha,(mx-55,my-55))
	aux.grilla(gbl.screen,gbl.cent)
	pygame.event.clear()
	pygame.time.set_timer(USEREVENT+1,2500)
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.MOUSEBUTTONDOWN:
				if event.button == 1: 
					tiempo_a = gbl.clock.tick()
					done = True
					position = event.pos
					button = event.button						
					if position[0] >= centro[0]-55 and position[0] <= centro[0]+55 and position[1] >= centro[1]-55 and position[1] <= centro[1]+55:
					#condicional que se encarga de verificar la posición del click.
						resultado_act = '1'
						play_sound('p')
					else:
						resultado_act = '0'
						play_sound('n')						
			elif event.type == USEREVENT+1: 
			#condiconal en cual cuando se acaba el tiempo aplica el sonido y se toma como respuesta incorrecto
				done = True
				play_sound('n')
				resultado_act = 'Tiempo maximo alcanzado'
				button = 'None'
				position = 'None'
				tiempo_a = '-1'
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_e:
					#Si se presiona la tecla E mientras se muestra una flecha se pasa a la pantalla de salida.
					cerrar()
					pygame.event.clear()
					pygame.mouse.set_visible(1)
					pygame.mouse.set_pos((mx,my))
					gbl.screen.fill(ctes.WHITE)
					gbl.screen.blit(flecha,(mx-55,my-55))
					aux.grilla(gbl.screen,gbl.cent)
		pygame.display.flip()
		
	gbl.screen.fill(ctes.WHITE)
	pygame.mouse.set_visible(0)
	aux.grilla(gbl.screen,gbl.cent)
	pygame.display.flip()
	if button=='None':
		pygame.event.pump()
		pygame.time.delay(int(c*2500))
	else:
		pygame.event.pump()
		pygame.time.delay(int(c*tiempo_a))
		
	return [str(resultado_act), tiempo_a, position]
	
	
def mostrar_estimulo(escogidos, numero, posicion_estimulo):
	'''
	Funcion que muestra un estimulo dentro de la grilla, junto a un enunciado con la instruccion. 
	Lo muestra por un periodo de 2000ms
	
	Use
	screen: display
	cent:	coordenadas del centro de la pantalla, 
	Escogidos: lista con todos los estimulos, 
	numer:  la posición del estimulo dentro de la lista que se va a mostrar, 
	posicion_estimulo: posición	dentro de la grilla donde se va a mostrar el estímulo.
	'''
	pygame.mouse.set_visible(0)
	estimulo = pygame.image.load(ctes.ESTIMULOS_PATH +  escogidos[numero])
	pygame.event.clear()
	gbl.screen.fill(ctes.WHITE)
	gbl.screen.blit(estimulo, (coord.identificar_coord(int(posicion_estimulo),gbl.cent)[0]-55,coord.identificar_coord(int(posicion_estimulo),gbl.cent)[1]-55))
	aux.grilla(gbl.screen,gbl.cent)
	pygame.display.flip()
	pygame.event.pump()
	pygame.time.delay(2000)

def recall(escogidos, numero, casilla_estimulo):
	'''
	Funcion encargada de preguntar por la posicion del estimulo codificado, no tiene timeout.
	Retorna el resultado del recall, el tiempo de respuesta y por ultimo la posicion de donde hizo clic.
	
	Use:
	screen: display 
	cent: coordenadas del centro de la pantalla
	escogidos:	lista que contiene los estimulos 
	numero: el numero dentro de la lista que corresponde al estimulo por el cual se va preguntar (mostrar), 
	casilla_estimulo: las coordenadas donde aparecio el estímulo.

		
	Retorna el 
		resultados_recall: resultado (1 = correcto, 0 = incorrecto),
		tiempo_r:  tiempo en responder 
		position:  posición de la respuesta
	'''
	pygame.event.clear()
	mx = gbl.cent[0]
	my = gbl.cent[1]
	
	pygame.mouse.set_visible(1)
	pygame.mouse.set_pos((mx,my))
	
	done = False	
	estimulo = pygame.image.load(ctes.ESTIMULOS_PATH +escogidos[numero])
	pregunta = pygame.image.load(ctes.PREGUNTA_PATH)
	text1 = gbl.font25.render(ctes.PREGUNTA_FIG_MSG,True,(0,0,0))
	text2 = gbl.font25.render(ctes.CLIC_FIG,True,(0,0,0))
	text1_width = text1.get_width()
	text2_width = text2.get_width()
	pygame.event.clear()
	pygame.mouse.set_visible(1)
	pygame.mouse.set_pos((mx,my))
	gbl.clock.tick()
	gbl.screen.fill(ctes.WHITE)
	gbl.screen.blit(estimulo,(coord.identificar_coord(13,gbl.cent)[0]-55, coord.identificar_coord(13,gbl.cent)[1]-55))
	gbl.screen.blit(pregunta,(coord.identificar_coord(14,gbl.cent)[0]-55, coord.identificar_coord(14,gbl.cent)[1]-55))
	gbl.screen.blit(text1,[mx-text1_width/2,my-325])
	gbl.screen.blit(text2,[mx-text2_width/2,my+300])
	aux.grilla(gbl.screen,gbl.cent)

	pygame.event.clear()
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.MOUSEBUTTONDOWN:
				if event.button == 1:
					tiempo_r = gbl.clock.tick()
					done = True
					position = event.pos
					button = event.button					
					if coord.identificar_casilla(position,gbl.cent) == casilla_estimulo:
					# se encarga de comparar si la posición de respuesta fue 
					#igual a la posición donde se mostro el estímulo
						resultados_recall = '1'
					else:
						resultados_recall ='0'
		pygame.display.flip()			
	return [resultados_recall, tiempo_r, position]

def feedback(demanda, escogidos, list_resultados_recall):
	'''
	Funcion encargada de dar una retroalimentacion al partipante. 
	Muestra al estimulo por el que se preguntó y un check si fue correcto y una equis si fue incorrecto.
	Use:
	screen: display
	cent:  coordenadas del centro, 
	demanda: la demanda si es demanda 2 o 3
	escogidos:	lista que contiene los estimulos
	list_resultados_recall: una lista con los resultados de recall
	'''
	mx = gbl.cent[0]
	my = gbl.cent[1]
	
	check = pygame.image.load(ctes.CHECK_PATH)
	equis = pygame.image.load(ctes.EQUIS_PATH)
	igual = pygame.image.load(ctes.IGUAL_PATH)
	estimulo1 = pygame.image.load(ctes.ESTIMULOS_PATH + escogidos[0])
	if demanda == 2 or demanda == 3:
		estimulo2 = pygame.image.load(ctes.ESTIMULOS_PATH + escogidos[1])
		if demanda == 3:
			estimulo3 = pygame.image.load(ctes.ESTIMULOS_PATH + escogidos[2])
	
	pygame.mouse.set_visible(0)
	gbl.screen.fill(ctes.WHITE)
	resultados_im = []
	for i in list_resultados_recall:
		if i == '1':
			resultados_im.append(check)
		elif i == '0':
			resultados_im.append(equis)
	
	if demanda == 1:
		gbl.screen.blit(estimulo1,(coord.identificar_coord(12,gbl.cent)[0]-55, coord.identificar_coord(12,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(13,gbl.cent)[0]-55, coord.identificar_coord(13,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[0],(coord.identificar_coord(14,gbl.cent)[0]-55, coord.identificar_coord(14,gbl.cent)[1]-55))
	
	if demanda == 2:
		gbl.screen.blit(estimulo1,(coord.identificar_coord(7,gbl.cent)[0]-55, coord.identificar_coord(7,gbl.cent)[1]-55))
		gbl.screen.blit(estimulo2,(coord.identificar_coord(17,gbl.cent)[0]-55, coord.identificar_coord(17,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(8,gbl.cent)[0]-55, coord.identificar_coord(8,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(18,gbl.cent)[0]-55, coord.identificar_coord(18,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[0],(coord.identificar_coord(9,gbl.cent)[0]-55, coord.identificar_coord(9,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[1],(coord.identificar_coord(19,gbl.cent)[0]-55, coord.identificar_coord(19,gbl.cent)[1]-55))
	elif demanda == 3:
		gbl.screen.blit(estimulo1,(coord.identificar_coord(2,gbl.cent)[0]-55, coord.identificar_coord(2,gbl.cent)[1]-55))
		gbl.screen.blit(estimulo2,(coord.identificar_coord(12,gbl.cent)[0]-55, coord.identificar_coord(12,gbl.cent)[1]-55))
		gbl.screen.blit(estimulo3,(coord.identificar_coord(22,gbl.cent)[0]-55, coord.identificar_coord(22,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(3,gbl.cent)[0]-55, coord.identificar_coord(3,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(13,gbl.cent)[0]-55, coord.identificar_coord(13,gbl.cent)[1]-55))
		gbl.screen.blit(igual,(coord.identificar_coord(23,gbl.cent)[0]-55, coord.identificar_coord(23,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[0],(coord.identificar_coord(4,gbl.cent)[0]-55, coord.identificar_coord(4,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[1],(coord.identificar_coord(14,gbl.cent)[0]-55, coord.identificar_coord(14,gbl.cent)[1]-55))
		gbl.screen.blit(resultados_im[2],(coord.identificar_coord(24,gbl.cent)[0]-55, coord.identificar_coord(24,gbl.cent)[1]-55))
	
	text1 = gbl.font25.render(ctes.BARRA_ESP_CONT,True,(0,0,0))
	text1_width = text1.get_width()
	gbl.screen.blit(text1,[mx-text1_width/2,2*my-100])	
	
	pygame.display.flip()
	done = False
	pygame.event.clear()
	while not done:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_SPACE:
					done = True
	
	
def ensayos (dat):
	'''
	IMPORTANTE: TRIAL se define como la aparición del estimulo a recordar, seguido de los tres eventos de actualización.

	Funcion encargada de correr en si todo la prueba, recibe los datos del excel donde se encuentra la informacion de la prueba. 
	Ademas establece variables donde se van a guardar los datos para el excel con los resultados de la prueba. 
	Esta función utiliza a las funciones anteriores para la ejecucion de la prueba total. Retorna todos los datos obtenidos por cada ensayo.
	
	Use:
		
		screen: display, 
		cent: las coordenadas del centro, 
		dat:l lista que contiene todos los datos de cada ensayo, 
		archivo:  contiene datos generales. 
	
	Return: 
		ESTIMULOS:La lista con los estimulos del ensayo
		POS_ESTIMULOS:La lista con la posición donde se mostro el ensayo
		T_CODIFICACION: Ya no aplica 
		FLECHAS: lista con las flechas mostradas en la actualización
		RESULTADOS_ACT: lista con los resultados de la actualización
		T_ACT: El tiempo en responder para cada evento de actualización
		CASILLAS_ACT: las casillas donde la persona da click para cada evento de la actualización
		POSICION_CLICK_ACT: las coordenadas donde la persona da click para cada evento de la actualización
		VALIDEZ: la validez de cada trial del ensayo
		RESULTADOS_RECALL: lista con los resultados del recall del ensayo
		T_RECALL: lista con los tiempos en responder en el recall
		CASILLAS_RECALL:la casillas donde la persona responde para cada estimulo del ensayo
		POSICION_BOLA: la posición donde se va a mostrar la bola
		POSICION_CAMBIO: la posición hacia donde se va mover la bola 
	'''
	
	demanda = int(dat[1])
	carga = float(dat[3])
	
	#Estos son los datos que contiene la lista dat,son aquellos que estan comentados, 
	#los otros son listas donde se van a almacenar las respuestas
	ESTIMULOS = [dat[4],dat[15],dat[26],dat[37]] 		#lista con los nombres de los estimulos por mostrar 
	POS_ESTIMULOS = [dat[5],dat[16],dat[27],dat[38]]		#la posición en donde se van a mostrar
	T_CODIFICACION = ['null','null','null','null']		#tiempo que dura en codificar los estimulos (Ya no aplica)
	POSICION_BOLA = [[dat[6],dat[8],dat[10]],[dat[17],dat[19],dat[21]],[dat[28],dat[30],dat[32]],[dat[39],dat[41],dat[43]]]		 #las diferentes posiciones donde se va a mostrar la bola para cada ensayo
	POSICION_CAMBIO =  [[dat[7],dat[9],dat[11]],[dat[18],dat[20],dat[22]],[dat[29],dat[31],dat[33]],[dat[40],dat[42],dat[44]]]		#las posiciones hacia donde se moveria la bola en cada ensayo
	FLECHAS = [[dat[12],dat[13],dat[14]],[dat[23],dat[24],dat[25]],[dat[34],dat[35],dat[36]],[dat[45],dat[46],dat[47]]]		# lista que contiene las flechas que se van a mostrar en cada actualización del ensayo
	RESULTADOS_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]		#Se almacena los resultados de actualización en una lista
	T_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]		# se guardan los tiempos de respuesta para cada actualización
	CASILLAS_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]		#se guardan las posiciones, en casillas, de respuesta de cada actualización del ensayo
	#POSICION_CLICK_ACT = [['null','null','null'],['null','null','null'],['null','null','null'],['null','null','null']]
	POSICION_CLICK_ACT = [[['null','null'],['null','null'],['null','null']],[['null','null'],['null','null'],['null','null']],
	[['null','null'],['null','null'],['null','null']],[['null','null'],['null','null'],['null','null']]]		#se guardan las posiciones, en coordenadas, de respuesta de cada actualización del ensayo
	VALIDEZ = ['null','null','null','null']		#almacena si los trials del ensayo fueron validoz
	RESULTADOS_RECALL = ['null','null','null','null']#lista donde se van a guardar los resultados del recall
	T_RECALL = ['null','null','null','null']		#lista donde se almacena los tiempos de respuesta del recall
	CASILLAS_RECALL = ['null','null','null','null']		 #lista donde se almacen las casillas de respuesta del recall 
	
	#Se guardan las posiciones en coordenadas hacia donde debe apuntar la flecha
	posicion_escogidas_1 = [ctes.BANCO_POSICIONES[flecha] for flecha in FLECHAS[0]]
	if demanda == 2 or demanda == 3:
		posicion_escogidas_2 = [ctes.BANCO_POSICIONES[flecha] for flecha in FLECHAS[1]]
		if demanda == 3:
			posicion_escogidas_3 = [ctes.BANCO_POSICIONES[flecha] for flecha in FLECHAS[2]]
	
	#Se ejecutan los eventos de actualización
	mostrar_estimulo(ESTIMULOS, 0, POS_ESTIMULOS[0]) 

	#Se ejecutan los eventos de actualización
	act_1 = actualizacion(POSICION_BOLA[0],POSICION_CAMBIO[0],posicion_escogidas_1,1,carga)
	act_2 = actualizacion(POSICION_BOLA[0],POSICION_CAMBIO[0],posicion_escogidas_1,2,carga)
	act_3 = actualizacion(POSICION_BOLA[0],POSICION_CAMBIO[0],posicion_escogidas_1,3,carga)
	
	#Se guardan los distintos elementos devueltos de la función de actualización
	RESULTADOS_ACT[0] = [act_1[0],act_2[0],act_3[0]]
	T_ACT[0] = [act_1[1],act_2[1],act_3[1]]
	POSICION_CLICK_ACT[0] = [act_1[2],act_2[2],act_3[2]]
	CASILLAS_ACT[0] = [coord.identificar_casilla(act_1[2],gbl.cent),coord.identificar_casilla(act_2[2],gbl.cent),coord.identificar_casilla(act_3[2],gbl.cent)]
		
	i = 0
	#Trial se define como la aparición del estimulo a recordar, seguido de los tres eventos de actualización.
	#condicinal por el cual se identifica si el trial fue valido, esto significa si al menos dos eventos de actualización fueron correctos 
	for j in RESULTADOS_ACT[0]:
		if j == '1':
			i += 1
	if i >= 2:
		VALIDEZ[0] = '1'
	else:
		VALIDEZ[0] = '0'
		
	if demanda == 2 or demanda == 3:	#Ejecuta as mismas funciones para demanda 2 y 3 
		punto_de_fijacion('blanco')
		
		mostrar_estimulo(ESTIMULOS, 1, POS_ESTIMULOS[1])
		
		act_1 = actualizacion(POSICION_BOLA[1],POSICION_CAMBIO[1],posicion_escogidas_2,1,carga)
		act_2 = actualizacion(POSICION_BOLA[1],POSICION_CAMBIO[1],posicion_escogidas_2,2,carga)
		act_3 = actualizacion(POSICION_BOLA[1],POSICION_CAMBIO[1],posicion_escogidas_2,3,carga)
			
		RESULTADOS_ACT[1] = [act_1[0],act_2[0],act_3[0]]
		T_ACT[1] = [act_1[1],act_2[1],act_3[1]]
		POSICION_CLICK_ACT[1] = [act_1[2],act_2[2],act_3[2]]
		CASILLAS_ACT[1] = [coord.identificar_casilla(act_1[2],gbl.cent),coord.identificar_casilla(act_2[2],gbl.cent),coord.identificar_casilla(act_3[2],gbl.cent)]
		
		i = 0
		for j in RESULTADOS_ACT[1]:
			if j == '1':
				i += 1
		if i >= 2:
			VALIDEZ[1] = '1'
		else:
			VALIDEZ[1] = '0'

		#Si la demanda del ensayo es 3 ejecuta esta ciclo.		
		if demanda == 3:			#para demanda 3 vuelve a ejecutar las mismas funciones
			punto_de_fijacion('blanco')

			mostrar_estimulo(ESTIMULOS, 2, POS_ESTIMULOS[2])	 
		
			act_1 = actualizacion(POSICION_BOLA[2],POSICION_CAMBIO[2],posicion_escogidas_3,1,carga)
			act_2 = actualizacion(POSICION_BOLA[2],POSICION_CAMBIO[2],posicion_escogidas_3,2,carga)
			act_3 = actualizacion(POSICION_BOLA[2],POSICION_CAMBIO[2],posicion_escogidas_3,3,carga)
		
			RESULTADOS_ACT[2] = [act_1[0],act_2[0],act_3[0]]
			T_ACT[2] = [act_1[1],act_2[1],act_3[1]]
			POSICION_CLICK_ACT[2] = [act_1[2],act_2[2],act_3[2]]
			CASILLAS_ACT[2] = [coord.identificar_casilla(act_1[2],gbl.cent),coord.identificar_casilla(act_2[2],gbl.cent),coord.identificar_casilla(act_3[2],gbl.cent)]
		
			i = 0
			for j in RESULTADOS_ACT[2]:
				if j == '1':
					i += 1
			if i >= 2:
				VALIDEZ[2] = '1'
			else:
				VALIDEZ[2] = '0'
		
	#Se ejecuta la función de recall para el primer estímulo	
	rec = recall(ESTIMULOS, 0, int(POS_ESTIMULOS[0]))
	RESULTADOS_RECALL[0] = rec[0]
	T_RECALL[0] = rec[1]
	CASILLAS_RECALL[0] = coord.identificar_casilla(rec[2],gbl.cent)
	
	if demanda == 2 or demanda == 3:
		#Se ejecuta la funcón de recall para el segundo estímulo
		rec = recall(ESTIMULOS, 1, int(POS_ESTIMULOS[1]))
		RESULTADOS_RECALL[1] = rec[0]
		T_RECALL[1] = rec[1]
		CASILLAS_RECALL[1] = coord.identificar_casilla(rec[2],gbl.cent)
	
		if demanda == 3:
		#Se ejecuta la funcón de recall para el tercer estímulo
			rec = recall(ESTIMULOS, 2, int(POS_ESTIMULOS[2]))
			RESULTADOS_RECALL[2] = rec[0]
			T_RECALL[2] = rec[1]
			CASILLAS_RECALL[2] = coord.identificar_casilla(rec[2],gbl.cent)
	#Se da el feed back
	if demanda == 1:
		feedback(1, ESTIMULOS, [RESULTADOS_RECALL[0]])
	if demanda == 2:
		feedback(2, ESTIMULOS, [RESULTADOS_RECALL[0],RESULTADOS_RECALL[1]])
	if demanda == 3:
		feedback(3, ESTIMULOS, RESULTADOS_RECALL)
	
	return [ESTIMULOS, POS_ESTIMULOS, T_CODIFICACION, FLECHAS, RESULTADOS_ACT, T_ACT, CASILLAS_ACT, POSICION_CLICK_ACT, VALIDEZ, RESULTADOS_RECALL, T_RECALL, CASILLAS_RECALL,POSICION_BOLA,POSICION_CAMBIO]
	
	
	
